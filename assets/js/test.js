
$(document).ready(function () {
    if(questions) {
        var total = questions.length,
        question = 1,
        answerRight = 0,
        block = $('.test-questions__list');
        $('.questions-number__now').text(total);
        $('.test-questions__begin-test').on('click', function (e) {
            $('.test-questions__next-test').text('Следующий вопрос');
            e.preventDefault();
            $('.questions-number__total').text(total);
            questionStep();
            $('.test-questions').addClass('test-begin');
            $('.test-questions__main-info, .test-questions__actions').fadeOut('400');
            setTimeout(function () {
                $('.test-questions__questions').fadeIn('400')
            },410)
            initQuestion();
        })


        $(document).on("click",".test-questions__item", function(){
            if($('.test-questions__questions').is('.answered')) {
                return false;
            }
            var questionName = $(this).attr("data-name");
            var rightItem = $('.test-questions__item[data-name='+questions[question-1].right+']');
            rightItem.addClass('answer-right')
            rightItem.after("" +
                "<div class='test-questions__item-description'>"+questions[question-1].desc+"</div>");
            $('.test-questions__link-next-wrapper').addClass('active');
            $('.test-questions__questions').addClass('answered');
            if(questions[question-1].right !== questionName)  {
                $(this).addClass('answer-error');
            } else {
                answerRight++
            }
        });

        $('.test-questions__next-test').on('click', function (e) {
            e.preventDefault();
            nextQuestion();
        })

        $('.test-questions__try-again').on('click', function (e) {
            e.preventDefault();
            question = 1;
            answerRight = 0;
            $('.test-questions').addClass('test-begin');
            $('.test-questions__result').fadeOut('400');
            $('.test-questions__actions').fadeOut('400');
            $('.test-questions__link-next-wrapper').removeClass('active');
            setTimeout(function () {
                $('.test-questions').removeClass('test-end');
                $('.test-questions__next-test').text('Следующий вопрос');
            },400);
            $('.test-questions__questions').removeClass('answered');
            questionStep();
            initQuestion();
        })

        function nextQuestion() {
            if(question === total) {
                $('.test-questions__next-test').text('Показать результаты');
                resultTest();
                return
            }
            question++
            questionStep();
            $('.test-questions__questions').removeClass('answered');
            $('.test-questions__link-next-wrapper').removeClass('active');
            initQuestion()
        }


        function questionStep() {
            $('.questions-number-main .questions-number__now').text(question);
        }

        function initQuestion() {
            if(question === total) {
                $('.test-questions__next-test').text('Показать результаты');
            }
            $('.test-questions__questions').fadeOut('400');
            $('.test-questions__changes-picture').fadeOut('400');
            setTimeout(function () {
                block.html('');
                questions[question-1].answers.map(item => {
                    block.append("<div class='test-questions__item' data-name="+ item.name +">"+
                        "<div class='test-questions__item-name'>"+ item.name +"</div>" +
                        "<div class='test-questions__item-question'>"+ item.text +"</div>" +
                        "</div>");
                });
                if(questions[question-1].imgSource) {
                    $('.test-questions__img-source').text(questions[question-1].imgSource)
                } else {
                    $('.test-questions__img-source').text('')
                }
                $('.test-questions__question').text(questions[question-1].question)
                $('.test-questions__changes-picture').attr("src", questions[question-1].img);
                $('.test-questions__changes-picture').fadeIn('400');
                $('.test-questions__questions').fadeIn('400');
            },400)
        }

        function resultTest() {
            resultText();
            var nameTest = $('.test-questions__title').text(),
                titleTest = $('.test-questions__result-text').text();
            $('.test-questions__result .questions-number__now').text(answerRight);
            $('.test-questions__questions').fadeOut('400');
            $('.test-questions__changes-picture').fadeOut('400');
            $('.test-questions').addClass('test-end');
            $('meta[property="og:title"]').attr('content', nameTest + ' Правильных ответов: '+ answerRight+ ' из '+ total+'. ' + titleTest);
            setTimeout(function () {
                $('.test-questions').removeClass('test-begin');
                $('.test-questions__result').fadeIn('400');
                $('.test-questions__actions').fadeIn('400');
            },400)
        }

        function resultText() {
            var resultObj;
            if(result) {
                resultObj = Object.values(result)[0];
                for (var item of Object.values(result)) {
                    if(answerRight >= item.before) {
                        resultObj = item
                    }
                }
                $('.test-questions__result-text').text(resultObj.title);
                $('.test-questions__result-desc').html(resultObj.description)
            }
        }
    }
})




