document.addEventListener("DOMContentLoaded", function () {
  const sightSlider = new Swiper(".sightSlider", {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    navigation: {
      nextEl: ".sight__sliderNav_next",
      prevEl: ".sight__sliderNav_prev",
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1240: {
        slidesPerView: 4,
      },
    },
  });
});

tippy("[data-tippy-content]");
