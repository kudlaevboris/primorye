<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 25.06.2020
 * Time: 2:25
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <section class="sights">
        <div class="container">
            <h2>Достопримечательности региона</h2>
        </div>
        <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="sight <?=($key % 2 == 1 ? "sight--left" : "") ?>" <?=($key % 2 == 1 ? 'data-aos="fade-up" data-aos-offset="200" data-aos-delay="50"
                 data-aos-duration="1000" data-aos-easing="ease-in-out"' : 'data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="1000"
                 data-aos-easing="ease-in-out"') ?>>
                <div class="container">
                    <div class="sight__inner <?=($key % 2 == 1 ? "sight__inner--left" : "") ?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <div class="sight__picture-wrapper <?=($key % 2 == 1 ? "sight__picture-wrapper--left" : "") ?>">
                            <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                 srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x" width="614"
                                 height="432" data-aos="fade-up" data-aos-offset="200"
                                 data-aos-delay="50" data-aos-duration="1000" data-aos-easing="ease-in-out">
                            <p class="sight__text">
                                <span><?= $arItem["NAME"] ?></span>
                            </p>
                        </div>
                        <div class="sight__info <?=($key % 2 == 1 ? "sight__info--left" : "") ?>">
                            <p>
                                <?= $arItem["PREVIEW_TEXT"] ?>
                            </p>
                            <? if (!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                                <div class="sight__link-wrapper <?=($key % 2 == 1 ? "sight__link-wrapper--left" : "") ?>">
                                    <a class="sight__link" href="<?= trim($arItem["PROPERTIES"]["LINK"]["VALUE"]) ?>" aria-label="Читать подробнее"  <? if ($arItem["PROPERTIES"]["TARGET_BLANK"]["VALUE"] == "Y"): ?>target="_blank"<? endif; ?>>Читать подробнее</a>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </section>
<?php endif; ?>
