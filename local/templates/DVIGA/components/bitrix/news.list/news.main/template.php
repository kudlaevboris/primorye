<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 18.06.2020
 * Time: 0:55
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <section class="main__inner main__inner--news">
        <div class="main__title-wrapper main__title-wrapper--news">
            <h2 class="main__title main__title--news">Новости</h2>
            <a href="/news/" class="btn-show-more btn-show-more--main btn-show-more--tablet">Все новости</a>
        </div>
        <div class="news news--main swiper-container" id="news-slider">
            <ul class="news__list-content news__list-content--main swiper-wrapper">
                <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                        array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <li class="news__item news__item--main swiper-slide"
                        id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <a class="news-item" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                            <div class="news-item__inner">
                                <img class="news-item__image"
                                     src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="399"
                                     height="300"
                                     srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x"
                                     loading="lazy">
                            </div>
                            <span class="news-item__date"><svg style="width: 10px; height: 10px;"><use xlink:href="#icon-date"></use></svg><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
                            <p class="news-item__description"><?= $arItem["NAME"] ?></p>
                        </a>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
        <div class="slider-btn slider-btn--news">
            <button class="slider-btn slider-btn__prev" type="button" id="prev-news">
                <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
            <button class="slider-btn slider-btn__next" type="button" id="next-news">
                <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
        </div>
    </section>
<?php endif; ?>
