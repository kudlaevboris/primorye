<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 25.06.2020
 * Time: 4:43
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <section class="festivals container">
        <h2 class="festivals__title">События и праздники</h2>
        <div class="festivals__inner">
            <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="festival">
                    <? if (!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                    <a href="<?= trim($arItem["PROPERTIES"]["LINK"]["VALUE"]) ?>" class="festival__link"
                       <? if ($arItem["PROPERTIES"]["TARGET_BLANK"]["VALUE"] == "Y"): ?>target="_blank"<? endif; ?>>
                        <? endif; ?>
                        <div class="festival__picture-wrapper" data-aos="fade-up" data-aos-offset="200"
                             data-aos-delay="50" data-aos-duration="1000" data-aos-easing="ease-in-out">
                            <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                 srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x" width="506"
                                 height="294">
                        </div>
                        <div class="festival__inner" data-aos="fade-up" data-aos-offset="200" data-aos-delay="50"
                             data-aos-duration="1000" data-aos-easing="ease-in-out"
                             id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <h3>
                                <?= $arItem["NAME"] ?>
                            </h3>
                            <div class="festival__desc">
                                <p>
                                    <?= $arItem["PREVIEW_TEXT"] ?>
                                </p>
                            </div>
                            <div class="festival__time">
                                <span><?= $arItem["PROPERTIES"]["PLACEHOLDER_DATE"]["VALUE"] ?></span>
                                <p><?= $arItem["PROPERTIES"]["DATE"]["VALUE"] ?></p>
                            </div>
                        </div>
                        <? if (!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                    </a>
                <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
    </section>
<?php endif; ?>
