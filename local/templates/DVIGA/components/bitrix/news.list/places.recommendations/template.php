<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 26.06.2020
 * Time: 4:12
 */
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <div class="places-inner__related related">
        <h2 class="related__heading sub-heading"><?= getTranslation("COMPONENT_TEXT_30"); ?></h2>
        <a href="<?=(!empty($arParams["SECTION_URL"]) ? $arParams["SECTION_URL"] : $arParams["SEF_FOLDER"])?>"
           class="related__btn btn-show-more btn-show-more--narrow"><?= getTranslation("COMPONENT_TEXT_31"); ?></a>
        <div class="related__list places-list">
            <? foreach ($arResult["ITEMS"] as $arItem):
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="related__item restaurant">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="restaurant__link">
                        <div class="restaurant__picture-wrapper">
                            <img src="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?>"
                                 srcset="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?> 2x" width="399"
                                 height="300" alt="">
                        </div>
                        <div class="restaurant__location">
                            <svg width="13" height="19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="#mark"></use>
                            </svg>
                            <span class="restaurant__address"><?= $arItem["PROPERTIES"]["ADDRESS_"]["VALUE"] ?></span>
                        </div>
                        <div class="restaurant__desc">
                            <p class="restaurant__title"><?= $arItem["NAME"] ?></p>
                            <? foreach ($arItem["PROPERTIES"]["MARKS"]["VALUE"] as $mark) : ?>
                                <span class="restaurant__type"><?= $mark ?></span>
                            <? endforeach; ?>
                        </div>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<?php endif; ?>
