<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 26.06.2020
 * Time: 1:19
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <section class="main__inner main__inner--vue">
        <div class="main__vue-bg" data-aos="fade-up" data-aos-offset="200" data-aos-delay="50"
             data-aos-duration="1000" data-aos-easing="ease-in-out"></div>

        <div class="main__title-wrapper main__title-wrapper--vue">
            <h2 class="main__title main__title--vue"><?= getTranslation("COMPONENT_TEXT_23"); ?></h2>
            <div class="main__title-inner main__title-inner--vue">
                <p><?= getTranslation("COMPONENT_TEXT_22"); ?></p>
            </div>
        </div>
        <div data-slider="main-vue" class="main__slider main__slider--vue swiper-container" id="vue-slider">
            <div class="swiper-wrapper">
                <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                        array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <a href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>">
                            <div class="main__vue-img-wrapper">
                                <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                     srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x"
                                     width="613" height="389" alt="<?= $arItem["NAME"] ?>" loading="lazy">
                            </div>
                            <p class="main__vue-text">
                                <span><?= $arItem["NAME"] ?></span>
                            </p>
                        </a>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="slider-btn slider-btn--width-auto">
            <button class="slider-btn__prev" id="prev-vue" type="button">
                <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
            <button class="slider-btn__next" id="next-vue" type="button">
                <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
        </div>
    </section>
<?php endif; ?>
