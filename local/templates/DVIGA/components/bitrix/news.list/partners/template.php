<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 18.06.2020
 * Time: 1:18
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <section class="main__inner main__inner--partners">
        <div class="main__title-wrapper main__title-wrapper--partners">
            <h2 class="main__title"><?= getTranslation("COMPONENT_TEXT_26"); ?></h2>
        </div>
        <div class="swiper-container" id="partners-slider">
            <div class="swiper-wrapper">
                <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                        array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <? if (!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                        <a href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>"
                           class="main__link-partners">
                            <? else: ?>
                            <div class="main__link-partners">
                                <? endif; ?>
                                <div class="main__partners-img">
                                    <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                         srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x"
                                         width="130" height="118" alt="<?= $arItem["NAME"] ?>" loading="lazy">
                                </div>
                                <div class="main__partners-content">
                                    <b><?= $arItem["NAME"] ?></b>
                                    <span><?= $arItem["PREVIEW_TEXT"] ?></span>
                                </div>
                                <? if (empty($arItem["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                            </div>
                            <? else: ?>
                        </a>
                    <? endif; ?>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="slider-btn slider-btn--width-auto">
            <button class="slider-btn__prev" type="button" id="prev-partners">
                <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
            <button class="slider-btn__next" type="button" id="next-partners">
                <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
        </div>
    </section>
<?php endif; ?>
