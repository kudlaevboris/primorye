<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 26.06.2020
 * Time: 4:41
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <div class="news-inner__related related">
        <h2 class="related__heading sub-heading"><?= getTranslation("COMPONENT_TEXT_27"); ?></h2>
        <a href="<?= getRootPath() ?>events/"
           class="related__btn btn-show-more"><?= getTranslation("COMPONENT_TEXT_15"); ?></a>
        <div class="event-list event-list--related">
            <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="event">
                    <a class="event__link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <div class="event__inner" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <div class="event__picture-wrapper">
                                <img src="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?>"
                                     srcset="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?> 2x" width="183"
                                     height="253" alt="<?= $arItem["NAME"] ?>" loading="lazy">
                            </div>
                            <div class="event__info">
                                <div class="event__date">
                                    <svg width="12" height="11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#calendar"></use>
                                    </svg>
                                    <span><?= $arItem["DATE"] ?></span>
                                </div>
                                <p>
                                    <?= $arItem["NAME"] ?>
                                </p>
                                <div class="event__details">
                                    <? if (!empty($arItem["PROPERTIES"]["PLACE"]["VALUE"])): ?>
                                        <div class="event__location">
                                            <svg width="13" height="19" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="#mark"></use>
                                            </svg>
                                            <span class="event__address"><?= $arItem["PROPERTIES"]["PLACE"]["VALUE"] ?></span>
                                        </div>
                                    <? endif; ?>
                                    <? if (!empty($arItem["PROPERTIES"]["TIME"]["VALUE"])): ?>
                                        <div class="event__schedule">
                                            <svg class="event__schedule-icon" width="17" height="18" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="#clock"></use>
                                            </svg>
                                            <span class="event__time"><?= $arItem["PROPERTIES"]["TIME"]["VALUE"] ?></span>
                                        </div>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<?php endif; ?>
