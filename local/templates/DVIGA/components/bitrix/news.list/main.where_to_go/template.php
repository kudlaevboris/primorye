<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 26.06.2020
 * Time: 0:56
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <section class="main__inner main__inner--bg-pattern-gray">
        <h2 class="main__title main__title--leisure"><?= getTranslation("COMPONENT_TEXT_20"); ?></h2>
        <ul class="main__leisure-list">
            <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <li class="main__leisure-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <a class="vacation-spots" href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>">
                        <div class="vacation-spots__inner">
                            <img class="vacation-spots__image"
                                 src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="399" height="465"
                                 srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x" alt="<?= $arItem["NAME"] ?>">
                        </div>
                        <h3 class="vacation-spots__title"><?= $arItem["NAME"] ?></h3>
                        <p class="vacation-spots__description"><?= $arItem["PREVIEW_TEXT"] ?></p>
                        <span class="vacation-spots__decoration-text"><?= getTranslation("COMPONENT_TEXT_21"); ?>
                            <svg><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/img/sprite_auto.svg#icon-arrow-left"></use></svg>
                        </span>
                    </a>
                </li>
            <? endforeach; ?>
        </ul>
    </section>
<?php endif; ?>
