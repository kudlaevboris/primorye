<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 26.06.2020
 * Time: 4:12
 */
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <div class="container">
        <div class="related related--slider">
            <? if(!is_null($arParams["TITLE_RECOMMENDATION"])):?>
                <h2 class="related__title sub-heading"><?= $arParams["TITLE_RECOMMENDATION"]; ?></h2>
            <? else: ?>
                <h2 class="related__title sub-heading"><?= getTranslation("COMPONENT_TEXT_29"); ?></h2>
            <? endif; ?>
            <? if($arParams["KEEP_LINK_SECTION"] != "Y") :?>
                <a href="<?=(!empty($arParams["SECTION_URL"]) ? $arParams["SECTION_URL"] : $arParams["SEF_FOLDER"])?>" class="related__btn btn-show-more btn-show-more--narrow"><?= getTranslation("COMPONENT_TEXT_19"); ?></a>
            <? endif; ?>
            <div class="related__slider swiper-container">
                <div class="swiper-wrapper">
                    <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                            array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div class="swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <a class="trip" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                <div class="trip__inner-image">
                                    <img class="trip__image"
                                         src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                         srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x"
                                         width="614" height="367" alt="<?= $arItem["NAME"] ?>" loading="lazy">
                                </div>
                                <div class="trip__data">
                                    <? if (!empty($arItem["PROPERTIES"]["CITY"]["VALUE"])): ?>
                                        <span class="trip__place">
                                        <svg style="width: 10px; height: 14px;"><use
                                                    xlink:href="#icon-map-marker"></use></svg><?= $arItem["PROPERTIES"]["CITY"]["VALUE"] ?>
                                    </span>
                                    <? endif; ?>
                                    <? if (!empty($arItem["PROPERTIES"]["DISTANCE"]["VALUE"]) || !empty($arItem["PROPERTIES"]["TIME"]["VALUE"])): ?>
                                        <div class="trip__inner-data">
                                            <? if (!empty($arItem["PROPERTIES"]["DISTANCE"]["VALUE"])) : ?>
                                                <span class="trip__distance">
                                                <svg style="width: 14px; height: 16px; fill: none">
                                                    <use xlink:href="#icon-distance-flag"></use>
                                                </svg>
                                                <?= $arItem["PROPERTIES"]["DISTANCE"]["VALUE"] ?>
                                            </span>
                                            <? endif; ?>
                                            <? if (!empty($arItem["PROPERTIES"]["DISTANCE"]["VALUE"]) && !empty($arItem["PROPERTIES"]["TIME"]["VALUE"])) : ?>
                                                <span class="trip__decor">
                                              <svg style="width: 1px; height: 22px; fill: none; vertical-align: middle">
                                                <use xlink:href="#icon-vertical-line"></use>
                                              </svg>
                                            </span>
                                            <? endif; ?>
                                            <? if (!empty($arItem["PROPERTIES"]["TIME"]["VALUE"])) : ?>
                                                <span class="trip__time"><svg
                                                            style="width: 17px; height: 17px; fill: #ffffff"><use
                                                                xlink:href="#icon-clock"></use></svg>
                                                <?= $arItem["PROPERTIES"]["TIME"]["VALUE"] ?>
                                            </span>
                                            <? endif; ?>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <div class="trip__content">
                                    <b class="trip__title"><?= $arItem["NAME"] ?></b>
                                    <p class="trip__description">
                                        <?= $arItem["PREVIEW_TEXT"] ?>
                                    </p>
                                </div>
                            </a>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
            <div class="slider-btn slider-btn--width-auto">
                <button class="slider-btn__prev" type="button" id="excursions-prev">
                    <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#slider-arrow"></use>
                    </svg>
                </button>
                <button class="slider-btn__next" type="button" id="excursions-next">
                    <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#slider-arrow"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>
<?php endif; ?>
