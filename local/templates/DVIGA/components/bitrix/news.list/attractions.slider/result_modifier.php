<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 24.03.2021
 * Time: 23:40
 */
foreach ($arResult["ITEMS"] as &$arItem) {
    if (is_null($arItem['PREVIEW_PICTURE'])) {
        $arItem['PREVIEW_PICTURE']["SRC"] = IMAGE_NOT_FOUND;
    }
}