<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 24.03.2021
 * Time: 23:25
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php if (count($arResult["ITEMS"]) > 0) : ?>
    <div class="row d-flex sight__titleRow">
        <div class="h2 sight__title">Достопримеча&shy;тельности</div>
        <div class="sight__sliderNav">
            <div class="sight__sliderNavItem sight__sliderNav_prev">
                <svg width="7" height="11" viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.92984 9.77659L1.53906 5.38582L6.02027 0.906249" stroke-width="2"/>
                </svg>
            </div>
            <div class="sight__sliderNavItem sight__sliderNav_next">
                <svg width="7" height="11" viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1.07016 1.22341L5.46094 5.61418L0.979729 10.0938" stroke-width="2"/>
                </svg>
            </div>
        </div>
    </div>
    <div class="slider-container sightSlider">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <? foreach ($arResult["ITEMS"] as $arItem) : ?>
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="swiper-slide">
                    <div class="sightCard">
                        <div class="sightCard__image"
                             style="background-image:url('<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>')"></div>
                        <div class="sightCard__adres">
                            <svg width="13" height="18" class="sightCard__icon" viewBox="0 0 13 18" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M6.3125 0C2.83179 0 0 2.83179 0 6.3125C0 7.48852 0.32576 8.6362 0.942281 9.63189L5.95318 17.7069C6.04916 17.8616 6.21827 17.9556 6.40014 17.9556C6.40154 17.9556 6.40291 17.9556 6.40431 17.9556C6.58776 17.9541 6.75722 17.8572 6.85145 17.6998L11.7346 9.5466C12.3171 8.57195 12.625 7.45362 12.625 6.3125C12.625 2.83179 9.79321 0 6.3125 0ZM10.8318 9.00646L6.39218 16.419L1.83648 9.07759C1.32331 8.24882 1.04507 7.29269 1.04507 6.3125C1.04507 3.41191 3.41191 1.04507 6.3125 1.04507C9.21309 1.04507 11.5764 3.41191 11.5764 6.3125C11.5764 7.26362 11.3165 8.19531 10.8318 9.00646Z"
                                      fill="#0D1E44"/>
                                <path d="M6.3125 3.15625C4.57214 3.15625 3.15625 4.57214 3.15625 6.3125C3.15625 8.04174 4.5491 9.46875 6.3125 9.46875C8.09764 9.46875 9.46875 8.02273 9.46875 6.3125C9.46875 4.57214 8.05286 3.15625 6.3125 3.15625ZM6.3125 8.42368C5.1462 8.42368 4.20132 7.47565 4.20132 6.3125C4.20132 5.15226 5.15226 4.20132 6.3125 4.20132C7.47274 4.20132 8.42017 5.15226 8.42017 6.3125C8.42017 7.45867 7.49729 8.42368 6.3125 8.42368Z"
                                      fill="#0D1E44"/>
                            </svg>
                            <?= $arItem["PROPERTIES"]["CITY"]["VALUE"] ?>
                        </div>
                        <div class="sightCard__title"><?= $arItem["NAME"] ?></div>
                    </div>
                </a>
            <? endforeach; ?>
        </div>
    </div>
<?php endif; ?>