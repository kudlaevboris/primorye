<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 18.06.2020
 * Time: 1:32
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="main__travel swiper-container" id="top-slider">
    <div class="swiper-wrapper">
        <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="main__travel-item swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="main__travel-description">
                    <h2 class="main__title main__title--top" id="test-1-h2"><?= $arItem["NAME"] ?></h2>
                    <p id="test-1-p"><?= $arItem["PREVIEW_TEXT"] ?></p>
                    <? if (!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                        <a href="<?= trim($arItem["PROPERTIES"]["LINK"]["VALUE"]) ?>"
                           <? if ($arItem["PROPERTIES"]["TARGET_BLANK"]["VALUE"] == "Y"): ?>target="_blank"<? endif; ?>
                           class="btn-show-more btn-show-more--travel"><?= (empty($arItem["PROPERTIES"]["TEXT_BUTTON"]["VALUE"]) ? getTranslation("MORE") : $arItem["PROPERTIES"]["TEXT_BUTTON"]["VALUE"]) ?></a>
                    <? endif; ?>
                </div>
                <div class="main__travel-img"
                     style="background-image: url('<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>');"></div>
            </div>
        <? endforeach; ?>
    </div>
    <div class="slider-btn slider-btn--main-slaider">
        <div class="slider-btn__toggles">
            <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                <button data-slide-index="<?= ($key + 1) ?>"
                        class="slider-btn__toggle <? if ($key == 0) : ?>slider-btn__toggle--active<? endif; ?>"
                        type="button"
                        id="tog-<?= ($key + 1) ?>"></button>
            <? endforeach; ?>
        </div>
        <div class="slider-btn__arrows">
            <button class="slider-btn__prev" type="button" data-btn-main-slider="next" id="top-slider-prev">
                <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
            <button class="slider-btn__next" type="button" data-btn-main-slider="prev" id="top-slider-next">
                <svg style="width: 10px; height: 14px;" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
        </div>
    </div>
</div>
