<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 24.06.2020
 * Time: 23:41
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <div class="gallery swiper-container" id="about-gallery">
        <ul class="gallery__list swiper-wrapper">
            <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <li class="gallery__item swiper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                         srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x" width="830"
                         height="483" alt="">
                </li>
            <? endforeach; ?>
        </ul>
        <div class="gallery__wrapper-button">
            <button class="gallery__btn-prev" type="button">
                <svg style="width: 10px; height: 14px;" fill="none">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
            <button class="gallery__btn-next" type="button">
                <svg style="width: 10px; height: 14px;" fill="none">
                    <use xlink:href="#slider-arrow"></use>
                </svg>
            </button>
        </div>
    </div>
<?php endif; ?>
