<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 19.06.2020
 * Time: 14:01
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>
    <div class="main__inner main__inner--virtual">
        <ul class="main__virtual-list">
            <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <li class="main__virtual-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <? if (!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                    <a href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>"
                       <? if ($arItem["PROPERTIES"]["TARGET_BLANK"]["VALUE"] == "Y"): ?>target="_blank"<? endif; ?>
                       class="main__virtual-link">
                        <? else: ?>
                        <div class="main__virtual-link">
                            <? endif; ?>
                            <? if (!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                                <div class="main__virtual-svg">
                                    <svg width="15" height="15" fill="none">
                                        <use href="#icon-link-to"></use>
                                    </svg>
                                </div>
                            <? endif; ?>
                            <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                 srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x" width="399" height="250"
                                 alt="<?= $arItem["NAME"] ?>" loading="lazy">
                            <? if (empty($arItem["PROPERTIES"]["LINK"]["VALUE"])) : ?>
                        </div>
                        <? else: ?>
                    </a>
                <? endif; ?>
                </li>
            <? endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
