<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 13.06.2020
 * Time: 16:46
 */
foreach ($arResult["ITEMS"] as &$arItem) {
    if (is_null($arItem['PREVIEW_PICTURE'])) {
        $arItem['PREVIEW_PICTURE']["SRC"] = IMAGE_NOT_FOUND;
    }

    if (empty($arItem["ACTIVE_TO"])) {
        $arItem["DATE"] = ConvertDateTime($arItem["ACTIVE_FROM"], "DD.MM");
    } else {
        $arItem["DATE"] = ConvertDateTime($arItem["ACTIVE_FROM"],
                "DD.MM") . " - " . $arItem["DATE"] = ConvertDateTime($arItem["ACTIVE_TO"], "DD.MM");
    }
}
