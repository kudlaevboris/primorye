<?php
foreach ($arResult["ITEMS"] as &$arItem) {
    if (is_null($arItem['PREVIEW_PICTURE'])) {
        $arItem['PREVIEW_PICTURE']["SRC"] = IMAGE_NOT_FOUND;
    }
}
