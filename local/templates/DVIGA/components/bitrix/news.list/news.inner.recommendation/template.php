<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 18.06.2020
 * Time: 0:55
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult['ITEMS']) > 0) : ?>

    <div class="news-inner__related related">
        <h2 class="related__heading sub-heading"><?= getTranslation("COMPONENT_TEXT_2"); ?></h2>
        <ul class="related__list">
            <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <li class="related__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <a class="news-item" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <div class="news-item__inner">
                            <img class="news-item__image"
                                 src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="399"
                                 height="300"
                                 srcset="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?> 2x"
                                 loading="lazy">
                        </div>
                        <span class="news-item__date"><svg style="width: 10px; height: 10px;"><use
                                        xlink:href="#icon-date"></use></svg><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
                        <p class="news-item__description"><?= $arItem["NAME"] ?></p>
                    </a>
                </li>
            <? endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
