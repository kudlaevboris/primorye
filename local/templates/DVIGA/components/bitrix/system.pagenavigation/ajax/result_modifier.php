<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 14.06.2020
 * Time: 03:44
 */

$arResult["AJAX_LINK"] = SITE_SERVER_PROTOCOL . SITE_SERVER_NAME . $APPLICATION->GetCurPage() . "?ajax=y";

if (stripos($APPLICATION->GetCurPage(), "/news/") !== false) {
    $arResult["dataType"] = "news";
} elseif (stripos($APPLICATION->GetCurPage(), "/tours/") !== false) {
    $arResult["dataType"] = "tours";
} elseif (stripos($APPLICATION->GetCurPage(), "/places/") !== false) {
    $arResult["dataType"] = "restaurant";
} elseif (stripos($APPLICATION->GetCurPage(), "/events/") !== false) {
    $arResult["dataType"] = "events";
} elseif (stripos($APPLICATION->GetCurPage(), "/sights/") !== false) {
    $arResult["dataType"] = "tours";
}else{
    $arResult["dataType"] = "restaurant";
}
