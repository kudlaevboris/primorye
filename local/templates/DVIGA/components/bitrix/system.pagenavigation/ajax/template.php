<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
    <div class="js-show-more-msg" hidden=""></div>
    <div class="show-more">
        <button class="show-more__btn btn-show-more js-more" data-url="<?= $arResult["AJAX_LINK"] ?>"
                data-type="<?= $arResult["dataType"] ?>"
                data-current="<?= intval($arResult["NavPageNomer"])?>"
                data-count="<?= intval($arResult["NavPageCount"]) ?>"
                type="button">Показать ещё
        </button>
    </div>
<?php endif; ?>
