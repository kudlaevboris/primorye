<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 15.06.2020
 * Time: 13:30
 */
?>
<label class="select select--dark">
    <span class="visually-hidden">Выберите город</span>
    <svg class="select__mark" width="10px" height="15px" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <use xlink:href="#mark-black"></use>
    </svg>
    <select name="city" id="city-select">
        <option value="<?= $arParams["SEF_FOLDER"] ?>"
                <? if (is_null($arParams["THIS_SECTION_CODE"])) : ?>selected<? endif; ?>>
            Выбрать город
        </option>
        <? foreach ($arResult["SECTIONS"] as $section): ?>
            <? if (intval($section["ELEMENT_CNT"]) > 0): ?>
                <option value="<?= $section["CODE"] ?>"
                        <? if ($arParams["THIS_SECTION_CODE"] == $section["CODE"]): ?>selected<? endif; ?>><?= $section["NAME"] ?></option>
            <? endif; ?>
        <? endforeach; ?>
    </select>
    <svg class="select__arrow select__arrow--dark" width="12px" height="12px" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <use xlink:href="#arrow-bottom-black"></use>
    </svg>
</label>
