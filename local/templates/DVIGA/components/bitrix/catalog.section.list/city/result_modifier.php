<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 13.06.2020
 * Time: 16:46
 */
foreach ($arResult["SECTIONS"] as &$section) {
    $section["ELEMENT_CNT"] = CIBlockSection::GetSectionElementsCount($section["ID"], ["CNT_ACTIVE" => "N"]);
}
