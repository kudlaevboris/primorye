<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 19.07.2020
 * Time: 23:54
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if(!empty($arResult["PROPERTIES"]["FILE"]["VALUE"])) :?>
    <div class="events-head__download-wrapper">
        <a href="<?= CFile::GetPath($arResult["PROPERTIES"]["FILE"]["VALUE"]) ?>" download="" class="events-head__download">
            <div class="events-head__download-inner">
                <div class="events-head__download-icon">
                    <svg width="44" height="40" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#download-icon"></use>
                    </svg>
                </div>
                <p><?= getTranslation("COMPONENT_TEXT_7"); ?></p>
            </div>
        </a>
    </div>
<?php endif; ?>
