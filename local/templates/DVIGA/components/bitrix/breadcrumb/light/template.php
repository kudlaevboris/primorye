<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $APPLICATION;

//delayed function must return a string
if (empty($arResult)) {
    return "";
}

$strReturn = '<ul class="header-content__breadcrumbs breadcrumbs breadcrumbs--light">';
$itemSize = count($arResult);

for ($index = 0; $index < $itemSize; $index++) {
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    if ($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1) {
        if(checkEmptySection($arResult[$index]["LINK"])){
            $strReturn .= '<li class="breadcrumbs__item"><a class="breadcrumbs__link breadcrumbs__link--no-link" href="#">' . $title . '</a></li>';
        }else{
            $strReturn .= '<li class="breadcrumbs__item"><a class="breadcrumbs__link" href="' . $arResult[$index]["LINK"] . '">' . $title . '</a></li>';
        }
    } else {
        $strReturn .= '<li class="breadcrumbs__item breadcrumbs__item--active"><a class="breadcrumbs__link" href="#">' . $title . '</a></li>';
    }
}
$strReturn .= '</ul>';
return $strReturn;

