<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 19.03.2021
 * Time: 14:12
 */

?>
<main class="main">
    <div class="container container_search">
        <form class="search__wrapper mb-3">
            <input type="text" class="search__input" placeholder="<?= getTranslation("SEARCH_TITLE"); ?>" name="q" value="<?= htmlspecialchars($_GET['q']) ?>">
            <button type="submit" class="search__button"><?= getTranslation("SEARCH_TITLE"); ?></button>
        </form>
        <? if (!empty($_GET['q'])) : ?>
            <div class="d-flex flex-wrap mb-6 searchFrase">
                <div class="fz-24 fwb text-dark"><?= getTranslation("RESULT_BY_PHRASE"); ?>:&nbsp;</div>
                <div class="fz-16"><?= htmlspecialchars($_GET['q']) ?></div>
            </div>
        <? endif; ?>
        <?php if (count($arResult["SEARCH"]) == 0) : ?>
            <div class="d-flex flex-wrap mb-6 searchFrase">
                <div class="fz-24 fwb text-dark"><?= getTranslation("SEARCH_NOT_FOUND"); ?></div>
            </div>
        <? else: ?>
            <? foreach ($arResult["SEARCH"] as $arItem): ?>
                <div class="resultItem mb-3">
                    <div class="fz-22 fwb mb-1 text-dark"><a href="<?= $arItem["URL"] ?>" target="_blank"><?= $arItem["TITLE_FORMATED"] ?></a></div>
                    <ul class="header-content__breadcrumbs breadcrumbs breadcrumbs mb-1">
                        <?= $arItem["CHAIN_PATH"] ?>
                    </ul>
                    <div class="fz-18 text-dark">
                        <?= $arItem["BODY_FORMATED"] ?>
                        <a href="<?= $arItem["URL"] ?>" class="resultsItem__readMore" target="_blank">
                            <?= getTranslation("READ_MORE_2"); ?>
                        </a>
                    </div>
                </div>
            <? endforeach; ?>
        <? endif; ?>
    </div>
</main>
