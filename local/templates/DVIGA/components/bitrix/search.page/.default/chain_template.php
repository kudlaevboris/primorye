<?
//Navigation chain template
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arChainBody = array();
foreach ($arCHAIN as $item) {
    if (mb_strlen($item["LINK"]) < mb_strlen(SITE_DIR))
        continue;
    if ($item["LINK"] <> "")
        $arChainBody[] = '<li class="breadcrumbs__item"><a href="' . $item["LINK"] . '" class="breadcrumbs__link">' . htmlspecialcharsex($item["TITLE"]) . '</a></li>';
    else
        $arChainBody[] = '<li class="breadcrumbs__item breadcrumbs__item--active"><a href="' . $item["LINK"] . '" class="breadcrumbs__link">' . htmlspecialcharsex($item["TITLE"]) . '</a></li>';
}
return implode('</li>', $arChainBody);