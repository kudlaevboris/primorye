<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 24.03.2021
 * Time: 17:56
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="wrapper">
    <main>
        <section class="about">
            <div class="container">
                <h1 class="about__heading"> <?= $arResult["PROPERTIES"]["TITLE"]["VALUE"] ?></h1>
                <div class="about__inner">
                    <div class="about__text">
                        <?= $arResult["PREVIEW_TEXT"] ?>
                    </div>
                    <div class="about__info">
                        <? if (!empty($arResult["PROPERTIES"]["PECULIARITY_REGION"]["VALUE"])) : ?>
                            <div class="about__item">
                                <div class="about__icon-wrapper">
                                    <svg width="64" height="56.39" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#bridge"></use>
                                    </svg>
                                </div>
                                <div class="about__info-text">
                                    <p><?= $arResult["PROPERTIES"]["PECULIARITY_REGION"]["VALUE"] ?></p>
                                    <span><?= $arResult["PROPERTIES"]["PECULIARITY_REGION"]["DESCRIPTION"] ?></span>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTIES"]["POPULATION"]["VALUE"])) : ?>
                            <div class="about__item">
                                <div class="about__icon-wrapper">
                                    <svg width="73" height="46" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#people"></use>
                                    </svg>
                                </div>
                                <div class="about__info-text">
                                    <p><?= $arResult["PROPERTIES"]["POPULATION"]["VALUE"] ?></p>
                                    <span><?= getTranslation("INDEX_POPULATION"); ?></span>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTIES"]["TERRITORY"]["VALUE"])) : ?>
                            <div class="about__item">
                                <div class="about__icon-wrapper">
                                    <svg width="44" height="89" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#island"></use>
                                    </svg>
                                </div>
                                <div class="about__info-text">
                                    <p><?= $arResult["PROPERTIES"]["TERRITORY"]["VALUE"] ?></p>
                                    <span><?= getTranslation("INDEX_TERRITORY"); ?></span>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTIES"]["TIMEZONE"]["VALUE"])) : ?>
                            <div class="about__item">
                                <div class="about__icon-wrapper">
                                    <svg width="61.64" height="60.45" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#time-zone"></use>
                                    </svg>
                                </div>
                                <div class="about__info-text">
                                    <p><?= $arResult["PROPERTIES"]["TIMEZONE"]["VALUE"] ?></p>
                                    <span><?= getTranslation("INDEX_TIMELINE"); ?></span>
                                </div>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </section>
        <? if (!empty($arResult["PROPERTIES"]["SLIDER"]["VALUE"])) : ?>
            <div class="gallery swiper-container" id="about-gallery">
                <ul class="gallery__list swiper-wrapper">
                    <? foreach ($arResult["PROPERTIES"]["SLIDER"]["VALUE"] as $sliderId): ?>
                        <li class="gallery__item swiper-slide">
                            <img src="<?= CFile::GetPath($sliderId) ?>"
                                 srcset="<?= CFile::GetPath($sliderId) ?> 2x" width="830" height="483" alt="">
                        </li>
                    <? endforeach; ?>
                </ul>
                <div class="gallery__wrapper-button">
                    <button class="gallery__btn-prev" type="button">
                        <svg style="width: 10px; height: 14px;" fill="none">
                            <use xlink:href="#slider-arrow"></use>
                        </svg>
                    </button>
                    <button class="gallery__btn-next" type="button">
                        <svg style="width: 10px; height: 14px;" fill="none">
                            <use xlink:href="#slider-arrow"></use>
                        </svg>
                    </button>
                </div>
            </div>
        <? endif; ?>
        <? if (!empty($arResult["DETAIL_TEXT"])) : ?>
            <section class="city container">
                <div class="city__picture-wrapper">
                    <? if (!empty($arResult["PROPERTIES"]["IMAGE_CENTER_PAGE"]["VALUE"])) : ?>
                        <img src="<?= CFile::GetPath($arResult["PROPERTIES"]["IMAGE_CENTER_PAGE"]["VALUE"]) ?>"
                             srcset="<?= CFile::GetPath($arResult["PROPERTIES"]["IMAGE_CENTER_PAGE"]["VALUE"]) ?> 2x"
                             width="957" height="991"
                             alt="<?= $arResult["PROPERTIES"]["TITLE"]["VALUE"]; ?>">
                    <? endif; ?>
                </div>
                <div class="city__inner">
                    <div class="city__desc">
                        <?= $arResult["DETAIL_TEXT"] ?>
                    </div>
                    <? if (!empty($arResult["PROPERTIES"]["CLIMATE"]["VALUE"])) : ?>
                        <div class="city__climat">
                            <h2><?= getTranslation("ABOUT_TEXT5"); ?></h2>
                            <div class="climat">
                                <? foreach ($arResult["PROPERTIES"]["CLIMATE"]["VALUE"] as $key => $element) : ?>
                                    <div class="climat__item">
                                        <p><?= $element ?></p>
                                        <span><?= $arResult["PROPERTIES"]["CLIMATE"]["DESCRIPTION"][$key] ?></span>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>
                </div>
            </section>
        <? endif; ?>
        <?php
        if(!empty($arResult["PROPERTIES"]["SIGHTS"]["VALUE"])) {
            $iblock = getInfoblockByCode("REGION_ATTRACTIONS");
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "about.sight",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(),
                    "USE_FILTER" => "N",
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                    "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "100",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "",
                    "PARENT_SECTION" => $arResult["PROPERTIES"]["SIGHTS"]["VALUE"],
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "LINK",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "SORT",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER1" => "ASC",
                    "SORT_ORDER2" => "DESC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => ""
                ),
                $component,
                []
            );
        }
        ?>
        <?php
        $GLOBALS["eventsFilter"] = [
                [
                    "LOGIC" => "OR",
                    "PROPERTY_ALL_AREA_VALUE" => "Y",
                    "PROPERTY_AREA.ID" => $arResult["ID"]
                ]
        ];
        $iblock = getInfoblockByCode("EVENTS_ABOUT");
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "about.events",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(),
                "USE_FILTER" => "Y",
                "FILTER_NAME" => "eventsFilter",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "100",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "LINK",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "ID",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "DESC",
                "STRICT_SECTION_CHECK" => "N",
                "COMPONENT_TEMPLATE" => ""
            ),
            $component,
            []
        );
        ?>
        <? if (!empty($arResult["PROPERTIES"]["TITLE_BOTTOM_BLOCK"]["VALUE"])) : ?>
            <section class="route">
                <? if(!empty($arResult["PROPERTIES"]["BIG_IMAGES_BOTTOM_BLOCK"]["VALUE"])) :?>
                    <div class="route__background">
                        <img src="<?= CFile::GetPath($arResult["PROPERTIES"]["BIG_IMAGES_BOTTOM_BLOCK"]["VALUE"]) ?>"
                             srcset="<?= CFile::GetPath($arResult["PROPERTIES"]["BIG_IMAGES_BOTTOM_BLOCK"]["VALUE"]) ?> 2x"
                             width="1440"
                             height="579"
                             alt="<?= $arResult["PROPERTIES"]["TITLE_BOTTOM_BLOCK"]["VALUE"] ?>">
                    </div>
                <? endif; ?>
                <div class="route__title-wrapper container">
                    <h2 class="route__title">
                        <span><?= $arResult["PROPERTIES"]["TITLE_BOTTOM_BLOCK"]["VALUE"] ?></span>
                    </h2>
                </div>
                <div class="container">
                    <div class="route__inner">
                        <div class="route__text">
                            <?= $arResult["PROPERTIES"]["DESCRIPTION_BOTTOM_BLOCK"]["~VALUE"]["TEXT"] ?>
                        </div>
                        <? if(!empty($arResult["PROPERTIES"]["BIG_IMAGES_BOTTOM_BLOCK"]["VALUE"])) :?>
                            <div class="route__picture">
                                <img src="<?= CFile::GetPath($arResult["PROPERTIES"]["SMALL_IMAGES_BOTTOM_BLOCK"]["VALUE"]) ?>"
                                     srcset="<?= CFile::GetPath($arResult["PROPERTIES"]["SMALL_IMAGES_BOTTOM_BLOCK"]["VALUE"]) ?> 2x" width="397"
                                     height="595"
                                     alt="<?= getTranslation("ABOUT_TEXT16"); ?>">
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </section>
        <? endif; ?>
    </main>
</div>
