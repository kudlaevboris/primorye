<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 24.03.2021
 * Time: 22:18
 */

foreach($arResult["ITEMS"] as $arItem){
    $arResult["JSON"]["regions"][$arItem["CODE"]] = [
        "name" => $arItem["NAME"],
        "link" => $arItem["DETAIL_PAGE_URL"],
    ];
}