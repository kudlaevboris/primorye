<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 24.03.2021
 * Time: 17:56
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script data-skip-moving=true>
    var globalData = <?= json_encode($arResult["JSON"], JSON_UNESCAPED_UNICODE) ?>;
</script>
<main class="main">
    <div class="container" id="mapApp">
        <div class="d-flex regionMap">
            <div class="map__left">
                <a class="mapLink" :href="item.link" v-for="(item, id, index) in regions" v-if="index <= 16"
                   :class="hoverRegion == id ? 'active':''" @mouseenter="linkHover(id)"
                   @mouseleave="regionLeave($event)">{{item.name}}</a>
            </div>
            <div class="map__center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 753.7 728" id="svgmap">
                    <g :data-tippy-content="regions[index] ? regions[index].name : null" v-for="(item,index) in map"
                       v-html="item" @click="regionClick(index)" @mouseenter="regionHover($event, index)"
                       @mouseleave="regionLeave($event)" :class="hoverRegion == index ? 'active':''"></g>
                </svg>
            </div>
            <div class="map__right">
                <div class="map__rightInner">
                    <a class="mapLink" :href="item.link" v-for="(item, id, index) in regions" v-if="index > 16"
                       :class="hoverRegion == id ? 'active':''" @mouseenter="linkHover(id)"
                       @mouseleave="regionLeave($event)">{{item.name}}</a>
                </div>
            </div>
        </div>
        <?php
        global $arrFilterD;
        $arrFilterD = ["PROPERTY_SHOW_ON_PAGE_MAP_VALUE" => "Y"];
        $iblock = getInfoblockByCode("ATTRACTIONS");
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "attractions.slider",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(),
                "USE_FILTER" => "Y",
                "FILTER_NAME" => "arrFilterD",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "100",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "SHOW_ON_PAGE_MAP",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "ID",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "DESC",
                "STRICT_SECTION_CHECK" => "N",
                "COMPONENT_TEMPLATE" => ""
            ),
            $component,
            []
        );
        ?>
    </div>
</main>