<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 14.06.2020
 * Time: 3:50
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="wrapper tours-inner">
    <main class="">
        <div class="container">
            <section class="tours-inner--content content content--narrow">
                <? if (!empty($arResult["DETAIL_PICTURE"]["SRC"])) : ?>
                    <img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
                         srcset="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?> 2x"
                         alt="<?= $arResult["DETAIL_PICTURE"]["DESCRIPTION"] ?>">
                <? endif; ?>

                <?= $arResult["PROPERTIES"]["ONE_BLOCK_TEXT"]["~VALUE"]["TEXT"] ?>

                <? if (!empty($arResult["PROPERTIES"]["TOUR_FEATURES"]["VALUE"])) : ?>
                    <div class="route-info">
                        <ul class="route-info__list js-data-list" data-list-count="7">
                            <? foreach ($arResult["PROPERTIES"]["TOUR_FEATURES"]["VALUE"] as $number => $element) : ?>
                                <li class="route-info__item">
                                    <span class="route-info__name"><?= $element ?>:</span>
                                    <span class="route-info__decor"></span>
                                    <span class="route-info__info"><?= $arResult["PROPERTIES"]["TOUR_FEATURES"]["DESCRIPTION"][$number] ?></span>
                                </li>
                            <? endforeach; ?>
                        </ul>
                        <button class="route-info__btn btn-more-info btn-more-info--hidden"
                                data-open-name="Больше&nbsp;информации" data-close-name="Меньше&nbsp;информации">
                            <svg style="width: 24px; height: 24px;">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/img/sprite_auto.svg#icon-more-info"></use>
                            </svg>
                            <span>Больше информации</span>
                        </button>
                    </div>
                <? endif; ?>
                <?= $arResult["PROPERTIES"]["TWO_BLOCK_TEXT"]["~VALUE"]["TEXT"] ?>

                <? if (!empty($arResult["PROPERTIES"]["GALLERY"]["VALUE"])): ?>
                    <div class="lightbox-block  lightbox-block--tours">
                        <? foreach ($arResult["PROPERTIES"]["GALLERY_"]["VALUE"] as $img): ?>
                            <a class="lightbox-block__preview" href="<?= $img["SRC"] ?>"
                               data-fancybox="gallery" data-caption="<?= $img["DESCRIPTION"] ?>">
                                <img src="<?= $img["SRC"] ?>"
                                     srcset="<?= $img["SRC"] ?>"
                                     width="291" height="232" alt="<?= $img["DESCRIPTION"] ?>">
                            </a>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>

                <?= $arResult["PROPERTIES"]["THIRD_BLOCK_TEXT"]["~VALUE"]["TEXT"] ?>
                <? if (!empty($arResult["PROPERTIES"]["GALLERY_TOP_FILES_"]["VALUE"])): ?>
                    <div class="lightbox-block  lightbox-block--tours">
                        <? foreach ($arResult["PROPERTIES"]["GALLERY_TOP_FILES_"]["VALUE"] as $img): ?>
                            <a class="lightbox-block__preview" href="<?= $img["SRC"] ?>"
                               data-fancybox="gallery" data-caption="<?= $img["DESCRIPTION"] ?>">
                                <img src="<?= $img["SRC"] ?>"
                                     srcset="<?= $img["SRC"] ?>"
                                     width="291" height="232" alt="<?= $img["DESCRIPTION"] ?>">
                            </a>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["FILES"]["VALUE"])): ?>
                    <?php foreach ($arResult["PROPERTIES"]["FILES_"] as $document): ?>
                        <div class="download">
                            <a href="<?= $document["URL"] ?>" class="download__link" download="">
                                <svg width="34" height="36.3" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="#download"></use>
                                </svg>
                                <b><?= $document["NAME"] ?></b>
                                <span><?= mb_strtoupper($document["TYPE"]) ?>, <?= $document["SIZE"] ?></span>
                            </a>
                        </div>
                    <? endforeach; ?>
                <? endif; ?>
            </section>
        </div>
        <? if (!empty($arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"])) : ?>
            <div class="map">
                <?= html_entity_decode(str_replace("scroll=true", "scroll=false", $arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"])) ?>
            </div>
        <? endif; ?>
        <?
        $iblock = getInfoblockByCode("TOURS_AND_EXCURSIONS");

        if($arParams["ACCOUNT_SECTION_RECOMMENDATIONS"] == "Y"){
            $GLOBALS["toursFilter"] = ["!ID" => $arResult["ID"]];
            $propertiesComponent["SECTION_URL"] = $arResult["SECTION_URL"];
            $propertiesComponent["PARENT_SECTION"] = $arResult["IBLOCK_SECTION_ID"];
        }

        $propertiesComponent = [
            //                "SECTION_URL" => $arResult["SECTION_URL"],
            "INCLUDE_SUBSECTIONS" => "Y",
            "KEEP_LINK_SECTION" => "Y",
//                "PARENT_SECTION" => $arResult["IBLOCK_SECTION_ID"],
            "TYPE_TEMPLATE" => $template,
            "ADD_ELEMENT_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "BROWSER_TITLE" => "-",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "N",
            "CHECK_DATES" => "Y",
            "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
            "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
            "DETAIL_DISPLAY_TOP_PAGER" => "N",
            "DETAIL_FIELD_CODE" => array("", ""),
            "DETAIL_PAGER_SHOW_ALL" => "N",
            "DETAIL_PAGER_TEMPLATE" => "",
            "DETAIL_PAGER_TITLE" => "Страница",
            "DETAIL_PROPERTY_CODE" => array(
                "GALLERY",
                "ROUTES"
            ),
            "DETAIL_SET_CANONICAL_URL" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "IBLOCK_ID" => $iblock["IBLOCK_ID"],
            "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
            "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
            "LIST_FIELD_CODE" => array("", ""),
            "PROPERTY_CODE" => array("CITY", "DISTANCE", "TIME"),
            "MESSAGE_404" => "",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "NEWS_COUNT" => "8",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "ajax",
            "PAGER_TITLE" => "Новости",
            "PREVIEW_TRUNCATE_LEN" => "",
            "SEF_FOLDER" => "/things-to-do/tours/",
            "SEF_MODE" => "Y",
            "SEF_URL_TEMPLATES" => array(
                "detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
                "news" => "",
                "section" => "#SECTION_CODE#/"
            ),
            "SET_LAST_MODIFIED" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SORT_BY1" => "rand",
            "SORT_BY2" => "rand",
            "SORT_ORDER1" => "DESC",
            "SORT_ORDER2" => "DESC",
            "STRICT_SECTION_CHECK" => "N",
            "USE_CATEGORIES" => "N",
            "USE_FILTER" => "Y",
            "FILTER_NAME" => "toursFilter",
            "USE_PERMISSIONS" => "N",
            "USE_RATING" => "N",
            "USE_RSS" => "N",
            "USE_SEARCH" => "N",
            "USE_SHARE" => "N"
        ];

        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "tours.recommendation",
            $propertiesComponent
        ); ?>
    </main>
</div>
