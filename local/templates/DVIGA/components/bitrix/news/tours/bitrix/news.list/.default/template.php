<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 13.06.2020
 * Time: 16:33
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="header-content header-content--bg-pattern header-content--bg-img"
     <? if (!empty($arResult["SECTION"]["PATH"][0]["PICTURE"])): ?>style="background-image: url(<?= CFile::GetPath($arResult["SECTION"]["PATH"][0]["PICTURE"]) ?>);"<? endif; ?>>
    <div class="container">
        <?php
        $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "light",
            array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0",
            ),
            false
        );
        ?>
        <h1 class="header-content__title"><?= $arResult["SECTION"]["PATH"][0]["NAME"] ?></h1>
    </div>
</div>
<main class="tours container">
    <? if (!empty($arResult["SECTION"]["PATH"][0]["DESCRIPTION"])) : ?>
        <div class="pattern pattern--narrow">
            <?= $arResult["SECTION"]["PATH"][0]["DESCRIPTION"] ?>
        </div>
    <? endif; ?>
    <?php if (count($arResult["ITEMS"]) > 0): ?>
        <ul class="tours__list" id="tours-list">
            <? foreach ($arResult["ITEMS"] as $arItem):
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                    CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                    array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <li class="tours__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <a class="trip" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                        <div class="trip__inner-image">
                            <img class="trip__image" src="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?>"
                                 srcset="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?> 2x" width="614" height="367"
                                 alt="">
                        </div>
                        <div class="trip__data">
                            <? if (!empty($arItem["PROPERTIES"]["CITY"]["VALUE"])): ?>
                                <span class="trip__place">
                                    <svg style="width: 10px; height: 14px;">
                                        <use xlink:href="#icon-map-marker"></use>
                                    </svg>
                                    <?= $arItem["PROPERTIES"]["CITY"]["VALUE"] ?>
                                </span>
                            <? endif; ?>
                            <? if (!empty($arItem["PROPERTIES"]["DISTANCE"]["VALUE"]) || !empty($arItem["PROPERTIES"]["TIME"]["VALUE"])): ?>
                                <div class="trip__inner-data">
                                    <? if(!empty($arItem["PROPERTIES"]["DISTANCE"]["VALUE"])) :?>
                                        <span class="trip__distance">
                                            <svg style="width: 14px; height: 16px; fill: none">
                                                <use xlink:href="#icon-distance-flag"></use>
                                            </svg>
                                            <?= $arItem["PROPERTIES"]["DISTANCE"]["VALUE"] ?>
                                        </span>
                                    <? endif; ?>
                                    <? if(!empty($arItem["PROPERTIES"]["DISTANCE"]["VALUE"]) && !empty($arItem["PROPERTIES"]["TIME"]["VALUE"])) :?>
                                        <span class="trip__decor">
                                            <svg style="width: 1px; height: 22px; fill: none; vertical-align: middle">
                                                <use xlink:href="#icon-vertical-line"></use>
                                            </svg>
                                        </span>
                                    <? endif; ?>
                                    <? if(!empty($arItem["PROPERTIES"]["TIME"]["VALUE"])) :?>
                                        <span class="trip__time">
                                            <svg style="width: 17px; height: 17px; fill: #ffffff">
                                                <use xlink:href="#icon-clock"></use>
                                            </svg>
                                            <?= $arItem["PROPERTIES"]["TIME"]["VALUE"] ?>
                                        </span>
                                    <? endif; ?>
                                </div>
                            <? endif; ?>
                        </div>
                        <div class="trip__content">
                            <b class="trip__title"><?= $arItem["NAME"] ?></b>
                            <p class="trip__description">
                                <?= $arItem["PREVIEW_TEXT"] ?>
                            </p>
                        </div>
                    </a>
                </li>
            <? endforeach; ?>
        </ul>
        <?= $arResult["NAV_STRING"] ?>
        <template id="tour-template">
            <li class="tours__item">
                <a class="trip" href="#">
                    <div class="trip__inner-image">
                        <img class="trip__image" src="img/content/vlad-trip@1x.jpg"
                             srcset="img/content/vlad-trip@2x.jpg 2x"
                             width="614" height="367" alt="<?= getTranslation("COMPONENT_TEXT_9"); ?>">
                    </div>
                    <div class="trip__data">
                        <span class="trip__place">
                            <svg style="width: 10px; height: 14px;"><use xlink:href="#icon-map-marker"></use></svg>
                            <span><?= getTranslation("VALUE"); ?><?= getTranslation("VLADIVOSTOK"); ?></span>
                        </span>
                        <div class="trip__inner-data">
                            <span class="trip__distance">
                                <svg style="width: 14px; height: 16px; fill: none"><use xlink:href="#icon-distance-flag"></use></svg>
                                <span>10 <?= getTranslation("KM"); ?></span>
                            </span>
                            <span class="trip__decor">
                                <svg style="width: 1px; height: 22px; fill: none; vertical-align: middle"><use xlink:href="#icon-vertical-line"></use></svg>
                            </span>
                            <span class="trip__time">
                                <svg style="width: 17px; height: 17px; fill: #ffffff"><use xlink:href="#icon-clock"></use></svg>
                                <span>1,5 <?= getTranslation("H"); ?></span>
                            </span>
                        </div>
                    </div>
                    <div class="trip__content">
                        <h3 class="trip__title"><?= getTranslation("COMPONENT_TEXT_10"); ?></h3>
                        <p class="trip__description">
                        <?= getTranslation("COMPONENT_TEXT_11"); ?>
                        </p>
                    </div>
                </a>
            </li>
        </template>
    <?php endif; ?>
</main>
