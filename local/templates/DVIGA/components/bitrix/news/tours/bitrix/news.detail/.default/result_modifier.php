<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 13.06.2020
 * Time: 18:12
 */
foreach ($arResult['PROPERTIES']['GALLERY_TOP_FILES']['VALUE'] as $slider) {
    $arResult['PROPERTIES']['GALLERY_TOP_FILES_']['VALUE'][] = CFile::GetFileArray($slider);
}

foreach ($arResult['PROPERTIES']['GALLERY']['VALUE'] as $slider) {
    $arResult['PROPERTIES']['GALLERY_']['VALUE'][] = CFile::GetFileArray($slider);
}

foreach ($arResult["PROPERTIES"]["FILES"]["VALUE"] as $key => $idDocument) {
    $arFile = CFile::GetFileArray($idDocument);

    if ($arFile) {
        if (stripos($arFile["CONTENT_TYPE"], "/") !== false) {
            $arFile["CONTENT_TYPE"] = explode('/', $arFile["CONTENT_TYPE"])[1];
        }
    }

    $file = [
        "URL" => $arFile["SRC"],
        "NAME" => getFileNameWithoutExtension($arFile["ORIGINAL_NAME"]),
        "TYPE" => getExtension($arFile["ORIGINAL_NAME"]),
        "SIZE" => formatBytes(intval($arFile["FILE_SIZE"])),
        "DATE" => $arFile["TIMESTAMP_X"]->toString(new \Bitrix\Main\Context\Culture(array("FORMAT_DATETIME" => "DD.MM.YYYY"))),
        "DESCRIPTION" => $arFile["DESCRIPTION"]
    ];

    if (!is_null($arResult["PROPERTIES"]["NAME_FILES"]["VALUE"][$key]) && $arResult["PROPERTIES"]["NAME_FILES"]["VALUE"][$key] != " ") {
        $file["NAME"] = $arResult["PROPERTIES"]["NAME_FILES"]["VALUE"][$key];
        $file["URL"] = "/local/helpers/file_with_different_name.php?file=" . $idDocument . "&name=" . $file["NAME"];
    }

    $arResult["PROPERTIES"]["FILES_"][] = $file;
}

if (!empty($arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"])) {
    $arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"] = str_replace('type=', 'data-skip-moving="true" type=',
        $arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"]);
}

$this->__component->SetResultCacheKeys(array(
    "DETAIL_PICTURE",
    "DETAIL_TEXT"
));

global $APPLICATION;
$cp = $this->__component; // объект компонента
if (is_object($cp)) {
    // проброс свойства ANY в $arResult["PROPERTY"] для вывода в component_epilog
    $cp->arResult['ONE_BLOCK_TEXT'] = $arResult["PROPERTIES"]['ONE_BLOCK_TEXT']["~VALUE"]["TEXT"];
    $cp->SetResultCacheKeys(array('ONE_BLOCK_TEXT'));
}
