<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 13.06.2020
 * Time: 17:59
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="wrapper news-inner">
    <main class="">
        <div class="container">
            <div class="news-inner__tag-date tag-date">
                <svg class="tag-date__icon" width="10" height="10" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="#date-calendar"></use>
                </svg>
                <span><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
            </div>
            <section class="news-inner__content content content--narrow">
                <?= $arResult["DETAIL_TEXT"] ?>
                <? if (!empty($arResult["PROPERTIES"]["GALLERY"]["VALUE"])): ?>
                    <div class="lightbox-block lightbox-block--news">
                        <? foreach ($arResult["PROPERTIES"]["GALLERY_"]["VALUE"] as $img): ?>
                            <a class="lightbox-block__preview" href="<?= $img["SRC"] ?>"
                               data-fancybox="gallery" data-caption="<?= $img["DESCRIPTION"] ?>">
                                <img src="<?= $img["SRC"] ?>"
                                     srcset="<?= $img["SRC"] ?>"
                                     width="291" height="232" alt="<?= $img["DESCRIPTION"] ?>">
                            </a>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
                <? if (!empty($arResult["PROPERTIES"]["FILES"]["VALUE"])): ?>
                    <?php foreach ($arResult["PROPERTIES"]["FILES_"] as $document): ?>
                        <div class="download">
                            <a href="<?= $document["URL"] ?>" class="download__link" download="">
                                <svg width="34" height="36.3" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="#download"></use>
                                </svg>
                                <b><?= $document["NAME"] ?></b>
                                <span><?= mb_strtoupper($document["TYPE"]) ?>, <?= $document["SIZE"] ?></span>
                            </a>
                        </div>
                    <? endforeach; ?>
                <? endif; ?>
            </section>
            <?
            $GLOBALS["newsFilter"] = ["<DATE_ACTIVE_FROM" => $arResult["ACTIVE_FROM"], ">=SORT" => $arResult["SORT"]];
            $iblock = getInfoblockByCode("NEWS");
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "news.inner.recommendation",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "DETAIL_PICTURE",
                        1 => "ACTIVE_FROM",
                    ),
                    "USE_FILTER" => "Y",
                    "FILTER_NAME" => "newsFilter",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                    "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "3",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "LINK",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => ""
                )
            );
            ?>
            <?
            $GLOBALS["eventsFilter"] = [">=DATE_ACTIVE_FROM" => ConvertTimeStamp(time() - (86400), "FULL")];
            $iblock = getInfoblockByCode("POSTER");
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "news.inner.events",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "DETAIL_PICTURE",
                        1 => "ACTIVE_FROM",
                        2 => "ACTIVE_TO"
                    ),
                    "USE_FILTER" => "Y",
                    "FILTER_NAME" => "eventsFilter",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                    "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "2",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "LINK",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "rand",
                    "SORT_BY2" => "rand",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "DESC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => ""
                )
            );
            ?>
        </div>
    </main>
</div>
