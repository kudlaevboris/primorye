<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 13.06.2020
 * Time: 16:33
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<?php if (count($arResult["ITEMS"]) > 0): ?>
    <main class="container">
        <div class="news">
            <ul class="news__list-content" id="news-list">
                <? foreach ($arResult["ITEMS"] as $arItem):
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                        array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <li class="news__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <a class="news-item" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                            <div class="news-item__inner">
                                <img class="news-item__image"
                                     src="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?>"
                                     width="399"
                                     height="300"
                                     srcset="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?>"
                                     alt="">
                            </div>
                            <span class="news-item__date"><svg style="width: 10px; height: 10px; margin-right: 4px;"><use xlink:href="#icon-date"></use></svg>
                                <?= $arItem["DISPLAY_ACTIVE_FROM"] ?>
                            </span>
                            <p class="news-item__description"><?= $arItem["NAME"] ?></p>
                        </a>
                    </li>
                <? endforeach; ?>
            </ul>
            <?= $arResult["NAV_STRING"] ?>
        </div>
    </main>
    <template id="news-template">
        <li class="news__item">
            <a class="news-item" href="#">
                <div class="news-item__inner">
                    <img class="news-item__image" src="<?= SITE_TEMPLATE_PATH ?>/assets/img/content/item-news-1@1x.jpg"
                         width="399" height="300"
                         srcset="<?= SITE_TEMPLATE_PATH ?>/assets/img/content/item-news-1@2x.jpg 2x"
                         alt="новость про медицину">
                </div>
                <span class="news-item__date"><svg style="width: 10px; height: 10px; margin-right: 4px;"><use xlink:href="#icon-date"></use></svg>
                    <span>03.20.20</span>
                </span>
                <p class="news-item__description">
                    Медицинский туризм в Приморье занимает второе место в России
                </p>
            </a>
        </li>
    </template>
<?php endif; ?>
