<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 14.06.2020
 * Time: 16:58
 */
foreach ($arResult['PROPERTIES']['GALLERY']['VALUE'] as $slider) {
    $arResult['PROPERTIES']['GALLERY_']['VALUE'][] = CFile::GetFileArray($slider);
}

$arResult["PROPERTIES"]["SITE_"]["VALUE"] = str_replace("https://", "", $arResult["PROPERTIES"]["SITE"]["VALUE"]);
$arResult["PROPERTIES"]["SITE_"]["VALUE"] = str_replace("http://", "", $arResult["PROPERTIES"]["SITE_"]["VALUE"]);

if (!is_null($arResult["IBLOCK_SECTION_ID"])) {
    $entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock($arParams["IBLOCK_ID"]);
    $rsSection = $entity::getList(array(
        "filter" => array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ID" => $arResult["IBLOCK_SECTION_ID"],
            "ACTIVE" => "Y",
            "GLOBAL_ACTIVE" => "Y"
        ),
        "select" => array("ID", "NAME", "UF_ICO"),
    ));
    $arSection = $rsSection->Fetch();
    $arResult["IBLOCK_SECTION_NAME"] = $arSection["NAME"];
    $arResult["IBLOCK_SECTION_ICON"] = CFile::GetPath($arSection['UF_ICO']);
}

if (!empty($arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"])) {
    $arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"] = str_replace('type=', 'data-skip-moving="true" type=',
        $arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"]);
}

$this->__component->SetResultCacheKeys(array(
    "DETAIL_PICTURE",
    "DETAIL_TEXT",
));

global $APPLICATION;
$cp = $this->__component; // объект компонента
if (is_object($cp)) {
    // проброс свойства ANY в $arResult["PROPERTY"] для вывода в component_epilog
    $cp->arResult['FIRST_TEXT_BLOCK'] = $arResult["PROPERTIES"]['FIRST_TEXT_BLOCK']["~VALUE"]["TEXT"];
    $cp->SetResultCacheKeys(array('FIRST_TEXT_BLOCK'));
}
