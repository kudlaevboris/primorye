<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 14.06.2020
 * Time: 16:19
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="header-content header-content--bg-pattern header-content--bg-img"
     <? if (!empty($arResult["SECTION"]["PATH"][0]["PICTURE"])): ?>style="background-image: url(<?= CFile::GetPath($arResult["SECTION"]["PATH"][0]["PICTURE"]) ?>);"<? endif; ?>>
    <div class="container">
        <?php
        $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "light",
            array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0",
            ),
            false
        );
        ?>
        <h1 class="header-content__title"><?= $arResult["SECTION"]["PATH"][0]["NAME"] ?></h1>
        <? if (false) : ?>
            <form action="#" method="get" class="header-content__select select-form">
                <label class="select select--light">
                    <span class="visually-hidden">Выберите город</span>
                    <svg class="select__mark" width="10px" height="15px" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#icon-map-marker-light"></use>
                    </svg>

                    <select name="city" id="city-select">
                        <option value="empty">Выбрать город</option>
                        <option value="moscow">Москва</option>
                        <option value="yekaterinburg">Екатеринбург</option>
                        <option value="kazan">Казань</option>
                    </select>
                    <svg class="select__arrow" width="12px" height="12px" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#arrow-bottom"></use>
                    </svg>
                </label>
            </form>
        <? endif; ?>
    </div>
</div>
<div class="wrapper">
    <main class="places">
        <div class="container">
            <? if (!empty($arResult["SECTION"]["PATH"][0]["DESCRIPTION"])) : ?>
                <div class="pattern pattern--narrow">
                    <?= $arResult["SECTION"]["PATH"][0]["DESCRIPTION"] ?>
                </div>
            <? endif; ?>
            <? if (count($arResult["ITEMS"]) > 0): ?>
                <div class="places__list places-list" id="places-list">
                    <? foreach ($arResult["ITEMS"] as $arItem):
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                            CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                            array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div class="places-list__item restaurant" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="restaurant__link">
                                <div class="restaurant__picture-wrapper">
                                    <img src="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?>"
                                         srcset="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?> 2x" width="399"
                                         height="300" alt="">
                                </div>
                                <div class="restaurant__location">
                                    <svg width="13" height="19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#mark"></use>
                                    </svg>
                                    <span class="restaurant__address"><?= $arItem["PROPERTIES"]["ADDRESS_"]["VALUE"] ?></span>
                                </div>
                                <div class="restaurant__desc">
                                    <p class="restaurant__title"><?= $arItem["NAME"] ?></p>
                                    <? foreach ($arItem["PROPERTIES"]["MARKS"]["VALUE"] as $mark) : ?>
                                        <span class="restaurant__type"><?= $mark ?></span>
                                    <? endforeach; ?>
                                </div>
                            </a>
                        </div>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
            <?= $arResult["NAV_STRING"] ?>

        </div>
    </main>
    <template id="restaurant-template">
        <div class="places-list__item restaurant">
            <a href="#" class="restaurant__link">
                <div class="restaurant__picture-wrapper">
                    <img src="" srcset="" width="399" height="300" alt="Ресторан Супра">
                </div>
                <div class="restaurant__location">
                    <svg width="13" height="19" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="#mark"></use>
                    </svg>
                    <span class="restaurant__address"></span>
                </div>
                <div class="restaurant__desc">
                    <p class="restaurant__title"></p>
                    <!--        <span class="restaurant__type"></span>-->
                </div>
            </a>
        </div>
    </template>
</div>
