$(document).ready(function() {
    $("a.contacts__link-map").click(function() {
        var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top - 50;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 800);
        return false;
    });
});
