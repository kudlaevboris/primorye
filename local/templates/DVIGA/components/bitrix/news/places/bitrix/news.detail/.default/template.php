<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 14.06.2020
 * Time: 16:59
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="header-content header-content--bg-pattern header-content--bg-img"
     style="background-image: url('<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>');">
    <div class="container">
        <?php
        $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "light",
            array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0",
            ),
            false
        );
        ?>
        <h1 class="header-content__title"><?= $arResult["NAME"] ?></h1>
        <div class="header-content__category">
            <? if (!empty($arResult["IBLOCK_SECTION_ICON"])) : ?>
                <img src="<?= $arResult["IBLOCK_SECTION_ICON"] ?>" width="28px" height="28px">
            <? endif; ?>
            <span><?= $arResult["IBLOCK_SECTION_NAME"] ?></span>
        </div>
        <? if (!empty($arResult["PROPERTIES"]["MARKS"]["VALUE"])) : ?>
            <div class="header-content__tags">
                <span class="header-content__tag"><?= $arResult["PROPERTIES"]["MARKS"]["VALUE"][0] ?></span>
            </div>
        <? endif; ?>
    </div>
</div>
<div class="wrapper places-inner">
    <main class="">
        <div class="places-inner__info container">
            <div class="places-inner__left">
                <div class="contacts">
                    <p class="contacts__title"><?= getTranslation("COMPONENT_TEXT_34"); ?></p>
                    <div class="contacts__inner">
                        <? foreach ($arResult["PROPERTIES"]["ADDRESS"]["VALUE"] as $key => $address) : ?>
                            <div class="contacts__info">
                                <svg width="13" height="19" class="contacts__icon" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="#green-mark"></use>
                                </svg>
                                <span class="contacts__address"><?= $address ?></span>
                                <? if(($key + 1) == count($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]) && !empty($arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"])) :?>
                                    <br>
                                    <p class="contacts__map">
                                        <a class="contacts__link-map"
                                           href="#map"><?= getTranslation("COMPONENT_TEXT_35"); ?></a>
                                    </p>
                                <? endif; ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <div class="contacts__list">
                        <? if (!empty($arResult["PROPERTIES"]["PHONE"]["VALUE"])) : ?>
                            <div class="contacts__phone">
                                <svg width="16" height="16" class="contacts__icon" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="#phone"></use>
                                </svg>
                                <a class="contacts__link" href="tel:<?= $arResult["PROPERTIES"]["PHONE"]["VALUE"] ?>"
                                   aria-label="<?= getTranslation("COMPONENT_TEXT_36"); ?>">
                                    <?= $arResult["PROPERTIES"]["PHONE"]["VALUE"] ?>
                                </a>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTIES"]["SITE"]["VALUE"])) : ?>
                            <div class="contacts__site">
                                <svg width="17" height="17" class="contacts__icon" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="#green-arrow"></use>
                                </svg>
                                <a class="contacts__link" href="<?= $arResult["PROPERTIES"]["SITE"]["VALUE"] ?>"
                                   aria-label="<?= getTranslation("COMPONENT_TEXT_37"); ?>"
                                   target="_blank">
                                    <?= $arResult["PROPERTIES"]["SITE_"]["VALUE"] ?>
                                </a>
                            </div>
                        <? endif; ?>
                    </div>
                    <ul class="contacts__social">
                        <? if (!empty($arResult["PROPERTIES"]["INSTAGRAM"]["VALUE"])) : ?>
                            <li class="contacts__social-item">
                                <a href="<?= $arResult["PROPERTIES"]["INSTAGRAM"]["VALUE"] ?>"
                                   aria-label="<?= getTranslation("COMPONENT_TEXT_38"); ?>" target="_blank">
                                    <svg width="20" height="21" class="contacts__social-icon"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#insta"></use>
                                    </svg>
                                </a>
                            </li>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTIES"]["FACEBOOK"]["VALUE"])) : ?>
                            <li class="contacts__social-item">
                                <a href="<?= $arResult["PROPERTIES"]["FACEBOOK"]["VALUE"] ?>"
                                   aria-label="<?= getTranslation("COMPONENT_TEXT_39"); ?>"
                                   target="_blank">
                                    <svg width="9" height="20" class="contacts__social-icon"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#facebook"></use>
                                    </svg>
                                </a>
                            </li>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTIES"]["TRIPADVISOR"]["VALUE"])) : ?>
                            <li class="contacts__social-item">
                                <a href="<?= $arResult["PROPERTIES"]["TRIPADVISOR"]["VALUE"] ?>"
                                   aria-label="<?= getTranslation("COMPONENT_TEXT_40"); ?>" target="_blank">
                                    <svg width="26" height="16" class="contacts__social-icon"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="#tripadvisor"></use>
                                    </svg>
                                </a>
                            </li>
                        <? endif; ?>
                    </ul>
                </div>
            </div>

            <div class="places-inner__right content">
                <div class="pattern">
                    <?= $arResult["PROPERTIES"]["FIRST_TEXT_BLOCK"]["~VALUE"]["TEXT"] ?>
                </div>
                <? if (!empty($arResult["PROPERTIES"]["FIRST_TITLE_BLOCK_FEATURES"]["VALUE"]) ||
                    !empty($arResult["PROPERTIES"]["SECOND_TITLE_BLOCK_FEATURES"]["VALUE"]) ||
                    !empty($arResult["PROPERTIES"]["THIRD_TITLE_BLOCK_FEATURES"]["VALUE"]) ||
                    !empty($arResult["PROPERTIES"]["FOURTH_TITLE_BLOCK_FEATURES"]["VALUE"])): ?>
                    <div class="places-inner__chars details">
                        <? if (!empty($arResult["PROPERTIES"]["FIRST_TITLE_BLOCK_FEATURES"]["VALUE"])) : ?>
                            <div class="details__item">
                                <b class="details__title"><?= $arResult["PROPERTIES"]["FIRST_TITLE_BLOCK_FEATURES"]["VALUE"] ?></b>
                                <ul class="details__list js-data-list" data-list-count="5">
                                    <? foreach ($arResult["PROPERTIES"]["FIRST_BLOCK_FEATURES"]["VALUE"] as $property): ?>
                                        <li class="details__point"><?= $property ?></li>
                                    <? endforeach; ?>
                                </ul>
                                <button class="btn-more-info btn-more-info--hidden"
                                        data-open-name="<?= getTranslation("COMPONENT_TEXT_32"); ?>"
                                        data-close-name="<?= getTranslation("COMPONENT_TEXT_33"); ?>">
                                    <svg style="width: 24px; height: 24px;">
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/img/sprite_auto.svg#icon-more-info"></use>
                                    </svg>
                                    <span><?= getTranslation("COMPONENT_TEXT_32"); ?></span>
                                </button>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTIES"]["SECOND_TITLE_BLOCK_FEATURES"]["VALUE"])) : ?>
                            <div class="details__item">
                                <b class="details__title"><?= $arResult["PROPERTIES"]["SECOND_TITLE_BLOCK_FEATURES"]["VALUE"] ?></b>
                                <ul class="details__list js-data-list" data-list-count="5">
                                    <? foreach ($arResult["PROPERTIES"]["SECOND_BLOCK_FEATURES"]["VALUE"] as $property): ?>
                                        <li class="details__point"><?= $property ?></li>
                                    <? endforeach; ?>
                                </ul>
                                <button class="btn-more-info btn-more-info--hidden"
                                        data-open-name="<?= getTranslation("COMPONENT_TEXT_32"); ?>"
                                        data-close-name="<?= getTranslation("COMPONENT_TEXT_33"); ?>">
                                    <svg style="width: 24px; height: 24px;">
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/img/sprite_auto.svg#icon-more-info"></use>
                                    </svg>
                                    <span><?= getTranslation("COMPONENT_TEXT_32"); ?></span>
                                </button>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTIES"]["THIRD_TITLE_BLOCK_FEATURES"]["VALUE"])) : ?>
                            <div class="details__item">
                                <b class="details__title"><?= $arResult["PROPERTIES"]["THIRD_TITLE_BLOCK_FEATURES"]["VALUE"] ?></b>
                                <ul class="details__list js-data-list" data-list-count="5">
                                    <? foreach ($arResult["PROPERTIES"]["THIRD_BLOCK_FEATURES"]["VALUE"] as $property): ?>
                                        <li class="details__point"><?= $property ?></li>
                                    <? endforeach; ?>
                                </ul>
                                <button class="btn-more-info btn-more-info--hidden"
                                        data-open-name="<?= getTranslation("COMPONENT_TEXT_32"); ?>"
                                        data-close-name="<?= getTranslation("COMPONENT_TEXT_33"); ?>">
                                    <svg style="width: 24px; height: 24px;">
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/img/sprite_auto.svg#icon-more-info"></use>
                                    </svg>
                                    <span><?= getTranslation("COMPONENT_TEXT_32"); ?></span>
                                </button>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arResult["PROPERTIES"]["FOURTH_TITLE_BLOCK_FEATURES"]["VALUE"])) : ?>
                            <div class="details__item">
                                <b class="details__title"><?= $arResult["PROPERTIES"]["FOURTH_TITLE_BLOCK_FEATURES"]["VALUE"] ?></b>
                                <ul class="details__list js-data-list" data-list-count="5">
                                    <? foreach ($arResult["PROPERTIES"]["FOURTH_BLOCK_FEATURES"]["VALUE"] as $property): ?>
                                        <li class="details__point"><?= $property ?></li>
                                    <? endforeach; ?>
                                </ul>
                                <button class="btn-more-info btn-more-info--hidden"
                                        data-open-name="<?= getTranslation("COMPONENT_TEXT_32"); ?>"
                                        data-close-name="<?= getTranslation("COMPONENT_TEXT_33"); ?>">
                                    <svg style="width: 24px; height: 24px;">
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/img/sprite_auto.svg#icon-more-info"></use>
                                    </svg>
                                    <span><?= getTranslation("COMPONENT_TEXT_32"); ?></span>
                                </button>
                            </div>
                        <? endif; ?>
                    </div>
                <? endif; ?>

                <?= $arResult["PROPERTIES"]["SECOND_TEXT_BLOCK"]["~VALUE"]["TEXT"] ?>

                <? if (!empty($arResult["PROPERTIES"]["GALLERY"]["VALUE"])): ?>
                    <div class="lightbox-block lightbox-block--narrow">
                        <? foreach ($arResult["PROPERTIES"]["GALLERY_"]["VALUE"] as $img): ?>
                            <a class="lightbox-block__preview" href="<?= $img["SRC"] ?>"
                               data-fancybox="gallery" data-caption="<?= $img["DESCRIPTION"] ?>">
                                <img src="<?= $img["SRC"] ?>"
                                     srcset="<?= $img["SRC"] ?>"
                                     width="291" height="232" alt="<?= $img["DESCRIPTION"] ?>">
                            </a>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </div>
        </div>
        <? if (!empty($arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"])) : ?>
            <div class="map" id="map">
                <?= html_entity_decode(str_replace("scroll=true", "scroll=false", $arResult["PROPERTIES"]["CODES_MAPS"]["VALUE"])) ?>
            </div>
        <? endif; ?>

        <div class="container">
            <?
            $GLOBALS["placesFilter"] = ["!ID" => $arResult["ID"], "SECTION_ID" => $arResult["IBLOCK_SECTION_ID"]];
            $iblock = getInfoblockByCode("WHERE_TO_GO");
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "places.recommendations",
                array(
                    "SECTION_URL" => $arResult["SECTION_URL"],
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "DETAIL_PICTURE",
                        1 => "ACTIVE_FROM",
                        2 => "ACTIVE_TO"
                    ),
                    "USE_FILTER" => "Y",
                    "FILTER_NAME" => "placesFilter",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                    "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "3",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array("CITY", "DISTANCE", "TIME", "MARKS"),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "rand",
                    "SORT_BY2" => "rand",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "DESC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => ""
                )
            );
            ?>
        </div>
        <?
        $iblock = getInfoblockByCode("TOURS_AND_EXCURSIONS");
        //        $GLOBALS["toursFilter"] = ["!ID" => $arResult["ID"]];
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "tours.recommendation",
            array(
                "TITLE_RECOMMENDATION" => getTranslation("COMPONENT_TEXT_28"),
//                "SECTION_URL" => $arResult["SECTION_URL"],
                "INCLUDE_SUBSECTIONS" => "Y",
                "KEEP_LINK_SECTION" => "Y",
//                "PARENT_SECTION" => $arResult["IBLOCK_SECTION_ID"],
                "TYPE_TEMPLATE" => $template,
                "ADD_ELEMENT_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "BROWSER_TITLE" => "-",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "N",
                "CHECK_DATES" => "Y",
                "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                "DETAIL_DISPLAY_TOP_PAGER" => "N",
                "DETAIL_FIELD_CODE" => array("", ""),
                "DETAIL_PAGER_SHOW_ALL" => "N",
                "DETAIL_PAGER_TEMPLATE" => "",
                "DETAIL_PAGER_TITLE" => "Страница",
                "DETAIL_PROPERTY_CODE" => array(
                    "GALLERY",
                    "ROUTES"
                ),
                "DETAIL_SET_CANONICAL_URL" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                "LIST_FIELD_CODE" => array("", ""),
                "PROPERTY_CODE" => array("CITY", "DISTANCE", "TIME"),
                "MESSAGE_404" => "",
                "META_DESCRIPTION" => "-",
                "META_KEYWORDS" => "-",
                "NEWS_COUNT" => "8",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "ajax",
                "PAGER_TITLE" => "Новости",
                "PREVIEW_TRUNCATE_LEN" => "",
                "SEF_FOLDER" => "/things-to-do/tours/",
                "SEF_MODE" => "Y",
                "SEF_URL_TEMPLATES" => array(
                    "detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
                    "news" => "",
                    "section" => "#SECTION_CODE#/"
                ),
                "SET_LAST_MODIFIED" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "rand",
                "SORT_BY2" => "rand",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "DESC",
                "STRICT_SECTION_CHECK" => "N",
                "USE_CATEGORIES" => "N",
                "USE_FILTER" => "Y",
                "FILTER_NAME" => "toursFilter",
                "USE_PERMISSIONS" => "N",
                "USE_RATING" => "N",
                "USE_RSS" => "N",
                "USE_SEARCH" => "N",
                "USE_SHARE" => "N"
            )
        ); ?>
    </main>
</div>
