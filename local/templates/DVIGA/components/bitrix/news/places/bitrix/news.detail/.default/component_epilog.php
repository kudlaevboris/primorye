<?php
if (!empty(trim($arResult["FIRST_TEXT_BLOCK"]))) {
    $APPLICATION->SetPageProperty("description",
        mb_strimwidth(htmlspecialchars(strip_tags($arResult["FIRST_TEXT_BLOCK"])), 0, 170, "..."));
}
if (!is_null($arResult["DETAIL_PICTURE"])) {
    $APPLICATION->SetPageProperty("og:image",
        SITE_SERVER_PROTOCOL . SITE_SERVER_NAME . $arResult["DETAIL_PICTURE"]["SRC"]);
    $APPLICATION->SetPageProperty("og:image:width", $arResult["DETAIL_PICTURE"]["WIDTH"]);
    $APPLICATION->SetPageProperty("og:image:height", $arResult["DETAIL_PICTURE"]["HEIGHT"]);
}
