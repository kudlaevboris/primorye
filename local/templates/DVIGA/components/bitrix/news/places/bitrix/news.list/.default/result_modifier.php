<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 13.06.2020
 * Time: 16:46
 */
foreach ($arResult["ITEMS"] as &$arItem) {
    if (is_null($arItem['PREVIEW_PICTURE'])) {
        $arItem['PREVIEW_PICTURE']["SRC"] = IMAGE_NOT_FOUND;
    }

    if (!empty($arItem["PROPERTIES"]["ADDRESS"]["VALUE"])) {
        $arItem["PROPERTIES"]["ADDRESS_"]["VALUE"] = (count($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]) > 1 ? $arItem["PROPERTIES"]["ADDRESS"]["VALUE"][0] . " <b>и еще " . declOfNum((count($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]) - 1), ["адрес", "адреса", "адресов"]) . "</b>" : $arItem["PROPERTIES"]["ADDRESS"]["VALUE"][0]);
    }
}
