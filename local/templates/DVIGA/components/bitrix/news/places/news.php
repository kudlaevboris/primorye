<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if($arParams["WITHOUT_SECTIONS"] != "Y"){
    header("Location: " . getRootPath());
    exit();
}else{
    include_once "section.php";
}
