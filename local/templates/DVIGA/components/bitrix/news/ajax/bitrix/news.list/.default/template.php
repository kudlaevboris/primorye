<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
header('Content-Type: application/json');
$returnArray = [];
foreach ($arResult["ITEMS"] as $arItem) {
    $element = [];
    $element['id'] = $arItem["ID"];
    $element['title'] = $arItem["~NAME"];
    $element['time'] = $arItem["NAME"];
    $element['date'] = $arItem["DISPLAY_ACTIVE_FROM"];
    $element['description'] = strip_tags($arItem["PREVIEW_TEXT"]);
    $element['link'] = $arItem["DETAIL_PAGE_URL"];
    if (is_null($arItem['PREVIEW_PICTURE'])) {
        $arItem['PREVIEW_PICTURE']["SRC"] = IMAGE_NOT_FOUND;
    }
    $element['img'] = [
        $arItem['PREVIEW_PICTURE']["SRC"],
        $arItem['PREVIEW_PICTURE']["SRC"]
    ];
    if (!is_null($arItem["IBLOCK_SECTION_ID"])) {
        $rsSection = \Bitrix\Iblock\SectionTable::getList(array(
            'filter' => array(
                'IBLOCK_ID' => $arParams["IBLOCK_ID"],
                "ID" => $arItem["IBLOCK_SECTION_ID"],
                'ACTIVE' => 'Y',
                'GLOBAL_ACTIVE' => 'Y',
            ),
            'select' => array('ID', 'NAME'),
        ));

        while ($arSection = $rsSection->fetch()) {
            $element["section"] = $arSection["NAME"];
        }
    }
    if (!empty($arItem["PROPERTIES"]["CITY"]["VALUE"])) {
        $element["place"] = $arItem["PROPERTIES"]["CITY"]["VALUE"];
    }

    if (!empty($arItem["PROPERTIES"]["DISTANCE"]["VALUE"])) {
        $element["distance"] = $arItem["PROPERTIES"]["DISTANCE"]["VALUE"];
    }

    if (!empty($arItem["PROPERTIES"]["TIME"]["VALUE"])) {
        $element["time"] = $arItem["PROPERTIES"]["TIME"]["VALUE"];
    }

    if (!empty($arItem["PROPERTIES"]["PLACE"]["VALUE"])) {
        $element["address"] = $arItem["PROPERTIES"]["PLACE"]["~VALUE"];
    }

    if (!empty($arItem["PROPERTIES"]["ADDRESS"]["VALUE"])) {
        $element["address"] = (count($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]) > 1 ? $arItem["PROPERTIES"]["ADDRESS"]["VALUE"][0] . " <b>и еще " . declOfNum((count($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]) - 1), ["адрес", "адреса", "адресов"]) . "</b>" : $arItem["PROPERTIES"]["ADDRESS"]["VALUE"][0]);
    }

    if(empty($element["address"])){
        $element["address"] = "";
    }

    if (!empty($arItem["ACTIVE_TO"])) {
        $element["date_start"] = ConvertDateTime($arItem["ACTIVE_FROM"], "DD.MM") . " - ";
    } else {
        $element["date_start"] = ConvertDateTime($arItem["ACTIVE_FROM"], "DD.MM");
    }
    $element["date_end"] = ConvertDateTime($arItem["ACTIVE_TO"], "DD.MM");

    $returnArray[] = $element;
}

//$returnArray["count_page"] = (int)$arResult["NAV_RESULT"]->NavPageCount;
//$returnArray["element_count"] = (int)$arResult["NAV_RESULT"]->NavRecordCount;

echo json_encode($returnArray);
