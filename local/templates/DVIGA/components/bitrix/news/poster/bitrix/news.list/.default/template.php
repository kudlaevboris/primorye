<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 15.06.2020
 * Time: 01:25
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="wrapper">
    <main>
        <div class="events-head">
            <div class="events-head__content">
                <div class="container">
                    <div class="events-head__info-wrapper">
                        <div class="events-head__page-info-inner">
                            <div class="events-head__page-title-wrapper">
                                <svg class="events-head__calendar-icon" width="43.27" height="46.27" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="#events-calendar"></use>
                                </svg>
                                <span><?= getTranslation("COMPONENT_TEXT_5"); ?></span>
                            </div>
                            <p><?= getTranslation("COMPONENT_TEXT_6"); ?></p>
                        </div>
                        <?
                        $iblock = getInfoblockByCode("SINGLE_ELEMENTS");
                        if(!is_null($arResult["DOWNLOAD_FILE_ID"])){
                            $APPLICATION->IncludeComponent(
                                "bitrix:news.detail",
                                "simple.elements.file",
                                Array(
                                    "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                                    "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                                    "ELEMENT_ID" => $arResult["DOWNLOAD_FILE_ID"],
                                    "PROPERTY_CODE" => ["FILE"],
                                    "SET_TITLE" => "N",
                                    "ADD_ELEMENT_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "SET_BROWSER_TITLE" => "N",
                                    "SET_META_KEYWORDS" => "N",
                                    "SET_META_DESCRIPTION" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N"
                                )
                            );
                        }
                        ?>
                    </div>
                    <form action="#" method="get" class="events-head_select-wrapper select-form">
                        <?
                        $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "city",
                            array(
                                "CHECK_DATES" => "N",
                                "VIEW_MODE" => "TEXT",
                                "SHOW_PARENT_NAME" => "Y",
                                "IBLOCK_TYPE" => "",
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "SECTION_ID" => "",
                                "SECTION_CODE" => "",
                                "SECTION_URL" => "",
                                "COUNT_ELEMENTS" => "N",
                                "TOP_DEPTH" => "1",
                                "SECTION_FIELDS" => "",
                                "SECTION_USER_FIELDS" => "",
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "36000000",
                                "CACHE_NOTES" => "",
                                "CACHE_GROUPS" => "Y",
                                "THIS_SECTION_CODE" => $arResult["SECTION"]["PATH"][0]['CODE'],
                                "SEF_FOLDER" => $arParams["SEF_FOLDER"]
                            )
                        ); ?>
                        <label class="select select--dark">
                            <span class="visually-hidden"><?= getTranslation("COMPONENT_TEXT_8"); ?></span>
                            <svg class="select__mark" width="14px" height="13px" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="#calendar-black"></use>
                            </svg>
                            <select name="month" id="month-select">
                                <? foreach ($arResult["MONTH"] as $key => $month): ?>
                                    <option value="<?= $key ?>"
                                            <? if ($month["SELECTED"]): ?>selected<? endif; ?>><?= $month["VALUE"] ?></option>
                                <? endforeach; ?>
                            </select>
                            <svg class="select__arrow  select__arrow--dark" width="12px" height="12px" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="#arrow-bottom-black"></use>
                            </svg>
                        </label>
                    </form>
                </div>
            </div>
        </div>
        <div class="container">
            <section class="event-list" id="events-list">
                <? foreach ($arResult["ITEMS"] as $arItem):
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
                        CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"),
                        array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <div class="event" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <a class="event__link" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                            <div class="event__inner">
                                <div class="event__picture-wrapper">
                                    <img src="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?>"
                                         srcset="<?= $arItem['PREVIEW_PICTURE']["SRC"] ?> 2x"
                                         width="183" height="253" alt="<?= $arItem["NAME"] ?>">
                                </div>
                                <div class="event__info">
                                    <div class="event__date">
                                        <svg width="12" height="11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="#calendar"></use>
                                        </svg>
                                        <span><?= $arItem["DATE"] ?></span>
                                    </div>
                                    <p class="event__title">
                                        <?= $arItem["NAME"] ?>
                                    </p>
                                    <div class="event__details">
                                        <? if (!empty($arItem["PROPERTIES"]["PLACE"]["VALUE"])): ?>
                                            <div class="event__location">
                                                <svg width="13" height="19" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="#mark"></use>
                                                </svg>
                                                <span class="event__address"><?= $arItem["PROPERTIES"]["PLACE"]["VALUE"] ?></span>
                                            </div>
                                        <? endif; ?>
                                        <? if (!empty($arItem["PROPERTIES"]["TIME"]["VALUE"])): ?>
                                            <div class="event__schedule">
                                                <svg class="event__schedule-icon" width="17" height="18" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="#clock"></use>
                                                </svg>
                                                <span class="event__time"><?= $arItem["PROPERTIES"]["TIME"]["VALUE"] ?></span>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                <? endforeach; ?>
            </section>
            <?= $arResult["NAV_STRING"] ?>
        </div>
    </main>

    <template id="event-template">
        <div class="event">
            <a class="event__link" href="#">
                <div class="event__inner">
                    <div class="event__picture-wrapper">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/exhibition@1x.jpg"
                             srcset="<?= SITE_TEMPLATE_PATH ?>/assets/img/exhibition@2x.jpg 2x" width="183"
                             height="253" alt="Выставка цитрусовых">
                    </div>
                    <div class="event__info">
                        <div class="event__date">
                            <svg width="12" height="11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="#calendar"></use>
                            </svg>
                            <span>25 мар - 5 апр</span>
                        </div>
                        <p class="event__title">
                            Выставка цитрусовых во Владивостоке. Выставка цитрусовых Владивосток
                        </p>
                        <div class="event__details">
                            <div class="event__location">
                                <svg width="13" height="19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="#mark"></use>
                                </svg>
                                <span class="event__address">
              Ботанический сад, ул. Маковского, 142
            </span>
                            </div>
                            <div class="event__schedule">
                                <svg width="17" height="18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="#clock"></use>
                                </svg>
                                <span class="event__time">
                10:00 - 17:00
              </span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </template>
</div>
