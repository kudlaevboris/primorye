<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 15.06.2020
 * Time: 01:25
 */
foreach ($arResult["ITEMS"] as &$arItem) {
    if (is_null($arItem['PREVIEW_PICTURE'])) {
        $arItem['PREVIEW_PICTURE']["SRC"] = IMAGE_NOT_FOUND;
    }

    if (empty($arItem["ACTIVE_TO"])) {
        $arItem["DATE"] = ConvertDateTime($arItem["ACTIVE_FROM"], "DD.MM");
    } else {
        $arItem["DATE"] = ConvertDateTime($arItem["ACTIVE_FROM"],
                "DD.MM") . " - " . $arItem["DATE"] = ConvertDateTime($arItem["ACTIVE_TO"], "DD.MM");
    }
}

$arResult["MONTH"] = [
    "all" => ["VALUE" => "Выбрать месяц", "SELECTED" => false],
    "01" => ["VALUE" => "Январь", "SELECTED" => false],
    "02" => ["VALUE" => "Февраль", "SELECTED" => false],
    "03" => ["VALUE" => "Март", "SELECTED" => false],
    "04" => ["VALUE" => "Апрель", "SELECTED" => false],
    "05" => ["VALUE" => "Май", "SELECTED" => false],
    "06" => ["VALUE" => "Июнь", "SELECTED" => false],
    "07" => ["VALUE" => "Июль", "SELECTED" => false],
    "08" => ["VALUE" => "Август", "SELECTED" => false],
    "09" => ["VALUE" => "Сентябрь", "SELECTED" => false],
    "10" => ["VALUE" => "Октябрь", "SELECTED" => false],
    "11" => ["VALUE" => "Ноябрь", "SELECTED" => false],
    "12" => ["VALUE" => "Декабрь", "SELECTED" => false],
];

$changeMonth = false;

foreach ($arResult["MONTH"] as $key => $month) {
    if ($key == $_GET["month"]) {
        $arResult["MONTH"][$key]["SELECTED"] = true;
        $changeMonth = !$changeMonth;
        break;
    }
}

if (!$changeMonth) {
    $arResult["MONTH"]["all"]["SELECTED"] = true;
}

$iblock = getInfoblockByCode("SINGLE_ELEMENTS");

$res = \CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => $iblock["IBLOCK_ID"], "CODE" => "kalendar-sobytiy"),
    false,
    false,
    array('ID')
);

$row = $res->fetch();
if($row){
    $arResult["DOWNLOAD_FILE_ID"] = $row["ID"];
}
