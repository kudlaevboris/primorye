<?php
if (!empty(trim($arResult["DETAIL_TEXT"]))) {
    $APPLICATION->SetPageProperty("description",
        mb_strimwidth(htmlspecialchars(strip_tags($arResult["DETAIL_TEXT"])), 0, 170, "..."));
}
if (!is_null($arResult["DETAIL_PICTURE"])) {
    $APPLICATION->SetPageProperty("og:image",
        SITE_SERVER_PROTOCOL . SITE_SERVER_NAME . $arResult["DETAIL_PICTURE"]["SRC"]);
    $APPLICATION->SetPageProperty("og:image:width", $arResult["DETAIL_PICTURE"]["WIDTH"]);
    $APPLICATION->SetPageProperty("og:image:height", $arResult["DETAIL_PICTURE"]["HEIGHT"]);
}
