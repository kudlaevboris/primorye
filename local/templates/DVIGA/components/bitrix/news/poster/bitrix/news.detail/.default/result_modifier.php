<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 14.06.2020
 * Time: 16:58
 */
$arResult["PROPERTIES"]["BUY_TICKET_"]["VALUE"] = str_replace("https://", "",
    $arResult["PROPERTIES"]["BUY_TICKET"]["VALUE"]);
$arResult["PROPERTIES"]["BUY_TICKET_"]["VALUE"] = str_replace("http://", "",
    $arResult["PROPERTIES"]["BUY_TICKET_"]["VALUE"]);

if (empty($arResult["ACTIVE_TO"])) {
    $arResult["DATE"] = ConvertDateTime($arResult["ACTIVE_FROM"], "DD.MM.YYYY");
} else {
    $arResult["DATE"] = ConvertDateTime($arResult["ACTIVE_FROM"],
            "DD.MM.YYYY") . " — " . $arResult["DATE"] = ConvertDateTime($arResult["ACTIVE_TO"], "DD.MM.YYYY");
}

$this->__component->SetResultCacheKeys(array(
    "DETAIL_PICTURE",
    "DETAIL_TEXT"
));
