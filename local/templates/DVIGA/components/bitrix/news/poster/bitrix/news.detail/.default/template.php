<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 14.06.2020
 * Time: 16:59
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="wrapper">
    <main class="">
        <section class="event-card container">
            <? if (!empty($arResult["DETAIL_PICTURE"]["SRC"])) : ?>
                <div class="event-card__picture">
                    <img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
                         srcset="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?> 2x" width="395"
                         height="547" alt="<?= $arResult["DETAIL_PICTURE"]["DESCRIPTION"] ?>">
                </div>
            <? endif; ?>
            <div class="event-card__content content">
                <div class="event-card__details details">
                    <div class="details__item">
                        <b class="details__title"><?= getTranslation("DATE"); ?></b>
                        <ul class="details__list js-data-list" data-list-count="5">
                            <li class="details__point"><?= $arResult["DATE"] ?></li>
                        </ul>
                    </div>
                    <? if (!empty($arResult["PROPERTIES"]["TIME"]["VALUE"])) : ?>
                        <div class="details__item">
                            <b class="details__title"><?= getTranslation("TIME"); ?></b>
                            <ul class="details__list js-data-list" data-list-count="5">
                                <li class="details__point"><?= $arResult["PROPERTIES"]["TIME"]["VALUE"] ?></li>
                            </ul>
                        </div>
                    <? endif; ?>
                    <? if (!empty($arResult["PROPERTIES"]["PLACE"]["VALUE"])) : ?>
                        <div class="details__item">
                            <b class="details__title"><?= getTranslation("PLACE"); ?></b>
                            <ul class="details__list js-data-list" data-list-count="5">
                                <li class="details__point"><?= $arResult["PROPERTIES"]["PLACE"]["VALUE"] ?></li>
                            </ul>
                        </div>
                    <? endif; ?>
                    <? if (!empty($arResult["PROPERTIES"]["SALE"]["VALUE"])) : ?>
                        <div class="details__item">
                            <b class="details__title"><?= getTranslation("PRICE"); ?></b>
                            <ul class="details__list js-data-list" data-list-count="5">
                                <li class="details__point"><?= $arResult["PROPERTIES"]["SALE"]["VALUE"] ?></li>
                            </ul>
                        </div>
                    <? endif; ?>
                    <? if (!empty($arResult["PROPERTIES"]["BUY_TICKET"]["VALUE"])) : ?>
                        <div class="details__item">
                            <b class="details__title"><?= getTranslation("COMPONENT_TEXT_3"); ?></b>
                            <ul class="details__list js-data-list" data-list-count="5">
                                <li class="details__point">
                                    <a href="<?= $arResult["PROPERTIES"]["BUY_TICKET"]["VALUE"] ?>"
                                       class="details__link">
                                        <?= $arResult["PROPERTIES"]["BUY_TICKET_"]["VALUE"] ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    <? endif; ?>
                    <? foreach ($arResult["PROPERTIES"]["ADITIONAL_FEATURES"]["VALUE"] as $key => $prop): ?>
                        <div class="details__item">
                            <b class="details__title"><?= $prop ?></b>
                            <ul class="details__list js-data-list" data-list-count="5">
                                <li class="details__point"><?= $arResult["PROPERTIES"]["ADITIONAL_FEATURES"]["DESCRIPTION"][$key] ?></li>
                            </ul>
                        </div>
                    <? endforeach; ?>
                </div>

                <?= $arResult["DETAIL_TEXT"] ?>
            </div>


        </section>
        <div class="container">
            <?
            $GLOBALS["eventsFilter"] = [
                "!ID" => $arResult["ID"],
                "SECTION_ID" => $arResult["IBLOCK_SECTION_ID"],
                [
                    "LOGIC" => "OR",
                    ">=DATE_ACTIVE_FROM" => ConvertTimeStamp(time() - (86400), "FULL"),
                    ">=DATE_ACTIVE_TO" => ConvertTimeStamp(time(), "FULL"),
                ]
            ];
            $iblock = getInfoblockByCode("POSTER");
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "news.inner.events",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "DETAIL_PICTURE",
                        1 => "ACTIVE_FROM",
                        2 => "ACTIVE_TO"
                    ),
                    "USE_FILTER" => "Y",
                    "FILTER_NAME" => "eventsFilter",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                    "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "2",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "LINK",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "rand",
                    "SORT_BY2" => "rand",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "DESC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => ""
                )
            );
            ?>
        </div>
        <?
        $iblock = getInfoblockByCode("TOURS_AND_EXCURSIONS");
        //        $GLOBALS["toursFilter"] = ["!ID" => $arResult["ID"]];
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "tours.recommendation",
            array(
                "TITLE_RECOMMENDATION" => getTranslation("COMPONENT_TEXT_28"),
//                "SECTION_URL" => $arResult["SECTION_URL"],
                "INCLUDE_SUBSECTIONS" => "Y",
                "KEEP_LINK_SECTION" => "Y",
//                "PARENT_SECTION" => $arResult["IBLOCK_SECTION_ID"],
                "TYPE_TEMPLATE" => $template,
                "ADD_ELEMENT_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "BROWSER_TITLE" => "-",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "N",
                "CHECK_DATES" => "Y",
                "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                "DETAIL_DISPLAY_TOP_PAGER" => "N",
                "DETAIL_FIELD_CODE" => array("", ""),
                "DETAIL_PAGER_SHOW_ALL" => "N",
                "DETAIL_PAGER_TEMPLATE" => "",
                "DETAIL_PAGER_TITLE" => "Страница",
                "DETAIL_PROPERTY_CODE" => array(
                    "GALLERY",
                    "ROUTES"
                ),
                "DETAIL_SET_CANONICAL_URL" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => $iblock["IBLOCK_ID"],
                "IBLOCK_TYPE" => $iblock["IBLOCK_TYPE"],
                "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                "LIST_FIELD_CODE" => array("", ""),
                "PROPERTY_CODE" => array("CITY", "DISTANCE", "TIME"),
                "MESSAGE_404" => "",
                "META_DESCRIPTION" => "-",
                "META_KEYWORDS" => "-",
                "NEWS_COUNT" => "8",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "ajax",
                "PAGER_TITLE" => "Новости",
                "PREVIEW_TRUNCATE_LEN" => "",
                "SEF_FOLDER" => "/things-to-do/tours/",
                "SEF_MODE" => "Y",
                "SEF_URL_TEMPLATES" => array(
                    "detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
                    "news" => "",
                    "section" => "#SECTION_CODE#/"
                ),
                "SET_LAST_MODIFIED" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "rand",
                "SORT_BY2" => "rand",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "DESC",
                "STRICT_SECTION_CHECK" => "N",
                "USE_CATEGORIES" => "N",
                "USE_FILTER" => "Y",
                "FILTER_NAME" => "toursFilter",
                "USE_PERMISSIONS" => "N",
                "USE_RATING" => "N",
                "USE_RSS" => "N",
                "USE_SEARCH" => "N",
                "USE_SHARE" => "N"
            )
        ); ?>
    </main>
</div>
