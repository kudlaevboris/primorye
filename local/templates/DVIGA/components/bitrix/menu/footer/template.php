<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 20.06.2020
 * Time: 19:34
 */
?>
<?php foreach ($arResult as $key => $arItem) : ?>
    <?php if ($arItem["DEPTH_LEVEL"] == 1) : ?>
        <div class="footer__nav-item footer-item">
            <? if(count($arItem["SUBMENU"]) > 0) :?>
                <b class="footer-item__header <?= $arParams["CLASS_MODIFIER_TITLE_SECTION"] ?>"><?= $arItem["TEXT"] ?></b>
            <? else: ?>
                <a class="footer-item__header <?= $arParams["CLASS_MODIFIER_TITLE_SECTION"] ?>" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
            <? endif; ?>
            <ul class="footer-item__list">
                <? foreach ($arItem["SUBMENU"] as $link) : ?>
                    <li>
                        <a href="<?= $link["LINK"] ?>" class="footer-item__link"><?= $link["TEXT"] ?></a>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
