<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 20.06.2020
 * Time: 14:26
 */

foreach ($arResult as $key => $arItem) {
    if ($arItem["DEPTH_LEVEL"] == 1) {
        $rootTree = $key;
    } elseif ($arItem["DEPTH_LEVEL"] == 2) {
        $arResult[$rootTree]["DEPTH_CHILDREN"] = 1;
        $rootTwoLevel = $key;
    } elseif ($arItem["DEPTH_LEVEL"] == 3) {
        $arResult[$rootTree]["DEPTH_CHILDREN"] = 2;
        $arResult[$rootTwoLevel]["SUBMENU"][] = $arItem;
    }
}
