<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 19.06.2020
 * Time: 23:55
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? $multiMenu = false; ?>
<? foreach ($arResult as $key => $arItem): ?>
    <? if ($arItem["DEPTH_LEVEL"] == 1 && $multiMenu): ?>
        <? $multiMenu = false; ?>
        </div>
    <?php endif; ?>
    <? if ($arItem["DEPTH_LEVEL"] == 1 && $arItem["IS_PARENT"] && $arItem["DEPTH_CHILDREN"] == 2): ?>
        <? $multiMenu = true; ?>
        <div data-event-list="<?=$arParams["ROOT_MENU_TYPE"]?>-<?= $key ?>" class="all-links__wrapper all-links__wrapper--leisure  all-links__wrapper--leisure-fix">
    <?php endif; ?>
    <? if($arItem["DEPTH_LEVEL"] == 2 && $multiMenu) :?>
        <ul>
            <li class="all-links__title-item"><?= $arItem["TEXT"] ?></li>
            <? foreach ($arItem["SUBMENU"] as $link) :?>
                <li>
                    <a href="<?=$link["LINK"]?>">
                        <span><?=$link["TEXT"]?></span>
                    </a>
                </li>
            <? endforeach;?>
        </ul>
    <? endif; ?>
<?php endforeach; ?>
<? if($multiMenu): ?>
    </div>
<? endif; ?>
