<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? $multiMenu = false; ?>
<? foreach ($arResult as $key => $arItem): ?>
    <? if ($arItem["DEPTH_LEVEL"] == 1 && $multiMenu): ?>
        <? $multiMenu = false; ?>
        </ul>
    </div>
    <? endif; ?>
    <? if ($arItem["DEPTH_LEVEL"] == 1 && $arItem["IS_PARENT"] && $arItem["DEPTH_CHILDREN"] == 1): ?>
        <? $multiMenu = true; ?>
        <div data-event-list="<?=$arParams["ROOT_MENU_TYPE"]?>-<?= $key ?>" class="all-links__wrapper">
            <b class="all-links__title-item all-links__title-item--show-on"><?= $arItem["TEXT"] ?></b>
                <ul>
    <?php elseif($arItem["DEPTH_LEVEL"] == 1 && !$arItem["IS_PARENT"]): ?>
        <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
    <? endif; ?>
    <? if ($arItem["DEPTH_LEVEL"] == 2 && $multiMenu) : ?>
            <li>
                <a href="<?= $arItem["LINK"] ?>">
                    <span><?= $arItem["TEXT"] ?></span>
                </a>
            </li>
    <? endif; ?>
<?php endforeach; ?>
<? if($multiMenu): ?>
        </ul>
    </div>
<? endif; ?>