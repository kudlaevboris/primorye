<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 19.06.2020
 * Time: 23:55
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<ul class="header__list header__list-lower">
    <? foreach ($arResult as $key => $arItem): ?>
        <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
            <? if ($arItem["IS_PARENT"]) : ?>
                <li class="header__item" id="<?= $arParams["ROOT_MENU_TYPE"] ?>-<?= $key ?>">
                    <button data-event-link="<?= $arParams["ROOT_MENU_TYPE"] ?>-<?= $key ?>" class="header__item-link"
                            type="button">
                        <span><?= $arItem["TEXT"] ?></span>
                        <svg style="width: 7px; height: 7px; fill: none;">
                            <use xlink:href="#arrow-bottom-black"></use>
                        </svg>
                    </button>
                </li>
            <? else: ?>
                <li class="header__item">
                    <a class="header__item-link" href="<?= $arItem["LINK"] ?>">
                        <span><?= $arItem["TEXT"] ?></span>
                    </a>
                </li>
            <? endif; ?>
        <? endif; ?>
    <?php endforeach; ?>
</ul>
