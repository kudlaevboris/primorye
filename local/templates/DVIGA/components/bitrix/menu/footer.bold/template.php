<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 20.06.2020
 * Time: 19:34
 */
?>
<div class="footer__nav-item footer-item">
    <ul class="footer-item__list footer-item__list--other">
        <?php foreach ($arResult as $key => $arItem) : ?>
            <li>
                <a href="<?= $arItem["LINK"] ?>"
                   class="footer-item__link footer-item__link--b"><?= $arItem["TEXT"] ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
