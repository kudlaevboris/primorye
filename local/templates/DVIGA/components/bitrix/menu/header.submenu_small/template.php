<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 19.06.2020
 * Time: 23:55
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? $multiMenu = false; ?>
<? foreach ($arResult as $key => $arItem): ?>
    <? if ($arItem["DEPTH_LEVEL"] == 1 && $multiMenu): ?>
        <? $multiMenu = false; ?>
        </ul>
        </div>
    <? endif; ?>
    <? if ($arItem["DEPTH_LEVEL"] == 1 && $arItem["IS_PARENT"] && $arItem["DEPTH_CHILDREN"] == 1): ?>
        <? $multiMenu = true; ?>
        <div data-event-list="<?=$arParams["ROOT_MENU_TYPE"]?>-<?= $key ?>" class="all-links__wrapper <?=(!empty($arParams["CLASS_MODIFIER"]) ? $arParams["CLASS_MODIFIER"] : "all-links__wrapper--tourism")?>">
        <b class="all-links__title-item all-links__title-item--show-on"><?= $arItem["TEXT"] ?></b>
        <ul class="all-links__list all-links__list--hotels">
    <? endif; ?>
    <? if ($arItem["DEPTH_LEVEL"] == 2 && $multiMenu) : ?>
        <li class="all-links__item">
            <a href="<?= $arItem["LINK"] ?>">
                <span><?= $arItem["TEXT"] ?></span>
            </a>
        </li>
    <? endif; ?>
<?php endforeach; ?>
<? if($multiMenu): ?>
    </div>
<? endif; ?>
