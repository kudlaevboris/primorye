<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 13.06.2020
 * Time: 16:10
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<? if(showTypicalPage()): ?>
            </section>
        </div>
    </main>
</div>
<?php endif; ?>
<footer class="footer <? if (ERROR_404 == 'Y'): ?>footer--error<? endif; ?>">
    <div class="container">
        <div class="footer__nav">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "footer",
                array(
                    "ROOT_MENU_TYPE" => "footer_upper",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "2",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            ); ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "footer.bold",
                array(
                    "ROOT_MENU_TYPE" => "footer_lower_bold",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "1",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            ); ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "footer",
                array(
                    "CLASS_MODIFIER_TITLE_SECTION" => "footer-item__header--small",
                    "ROOT_MENU_TYPE" => "footer_lower",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "2",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            ); ?>
        </div>

        <div class="footer__main">
            <div class="footer__top">
                <a href="<?= getRootPath() ?>" class="footer__logo logo">
                    <svg width="217" height="41" fill="none">
                        <use href="#logo-color"></use>
                    </svg>
                </a>
                <div class="footer__contacts">
                    <p><?= getTranslation("FOOOTER_TEXT_1"); ?></p>
                    <a href="tel:+74232407121" class="footer__phone">+7&nbsp;(423)&nbsp;240-71-21</a>
                    <a href="info@visit-primorye.ru" class="footer__mail">info@visit-primorye.ru</a>
                </div>
            </div>
            <div class="footer__bottom">
                <ul class="footer__socials socials">
                    <li class="socials__item">
                        <a href="https://www.instagram.com/visit.primorye/" class="socials__link socials__link--instagram" target="_blank">
                            <span class="visually-hidden"><?= getTranslation("FOOOTER_TEXT_2"); ?></span>
                            <svg class="socials__icon" width="20" height="21" fill="none">
                                <use href="#instagram"></use>
                            </svg>
                        </a>
                    </li>
                    <li class="socials__item">
                        <a href="https://www.facebook.com/visit.primorye" class="socials__link" target="_blank">
                            <span class="visually-hidden"><?= getTranslation("FOOOTER_TEXT_3"); ?></span>
                            <svg class="socials__icon" width="18" height="18" fill="none">
                                <use href="#icon-fb"></use>
                            </svg>
                        </a>
                    </li>
                    <li class="socials__item">
                        <a href="https://vk.com/visit.primorye" class="socials__link" target="_blank">
                            <span class="visually-hidden"><?= getTranslation("FOOOTER_TEXT_4"); ?></span>
                            <svg class="socials__icon" width="26" height="15" fill="none">
                                <use href="#icon-vk"></use>
                            </svg>
                        </a>
                    </li>
                </ul>
                <a href="<?= getRootPath() ?>privacy-policy/" class="footer__policy"><?= getTranslation("FOOOTER_TEXT_6"); ?></a>
                <p class="footer__design">
                    <?= getTranslation("FOOOTER_TEXT_5"); ?>
                    <a href="https://dviga.marketing/" target="_blank">
                        <span class="visually-hidden"><?= getTranslation("FOOOTER_TEXT_7"); ?></span>
                        <svg class="footer__design-logo" width="76" height="11" fill="none">
                            <use href="#logo-dviga"></use>
                        </svg>
                    </a>
                </p>
            </div>
        </div>
    </div>
<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="https://unpkg.com/tippy.js@6"></script>
<script src="<? SITE_TEMPLATE_PATH?>/local/templates/DVIGA/assets/js/slider.js"></script>
</footer>
</body>
</html>
