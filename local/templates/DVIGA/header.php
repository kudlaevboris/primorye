<?php
/**
 * Created by PhpStorm.
 * User: DarkTraveler
 * Date: 13.06.2020
 * Time: 16:10
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/style.min.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/aos-style-min.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/fancybox-min.css');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/vendormin/jquery.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/vendormin/jquery.fancybox.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/vendor.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/main.min.js');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><? $APPLICATION->ShowTitle() ?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="<? SITE_TEMPLATE_PATH?>/local/templates/DVIGA/assets/js/scripts.js"></script>
    <? $APPLICATION->ShowHead(); ?>
    <? \Vlad\Classes\SeoController::showSocialMetaTags(); ?>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(67123228, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/67123228" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

</head>
<body>
<? $APPLICATION->ShowPanel(); ?>
<? include_once $_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . "/svg-sprite.php"; ?>
<? $APPLICATION->IncludeComponent("elcore:notification", ".default", array(), "", array("HIDE_ICONS" => "Y")); ?>
<header class="<?= (thisPageWithSectionImage() ? "header header--dark" : ($APPLICATION->GetCurPage() == SITE_DIR ? "header header--main-page" : "header")) ?>">
<div id="headerApp">

<div class="header__wrapper container">
        <a href="<?= getRootPath() ?>" class="header__logo logo">
            <svg width="215" height="41" fill="none">
                <use href="#logo-color"></use>
            </svg>
        </a>
        <nav class="header__menu-wrapper">
            <div class="header__wrapper-list">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "header.upper",
                    array(
                        "ROOT_MENU_TYPE" => "header_upper",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "3",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                ); ?>
                <? if (false) : ?>
                    <div class="header__version-a11y">
                        <a class="header__link-a11y" href="#">
                            <span><?= getTranslation("HEADER_TEXT_1"); ?></span>
                            <svg style="width: 19px; height: 15px;">
                                <use xlink:href="#icon-a11y"></use>
                            </svg>
                        </a>
                    </div>
                <? endif; ?>
            </div>
            <div class="header__wrapper-list header__wrapper-list--lower">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "header.lower",
                    array(
//                        "CLASS_MODIFIER" => "all-links__wrapper--hotels",
                        "ROOT_MENU_TYPE" => "header_lower",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "3",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                ); ?>
                <div class="header__search" @click="searchShow = !searchShow">
                    <svg style="width: 17px; height: 18px;">
                        <path d="M14.924 15.413l-4.08-4.37a6.01 6.01 0 001.686-4.19c0-3.275-2.586-5.939-5.765-5.939C3.585.914 1 3.578 1 6.852c0 3.275 2.586 5.939 5.765 5.939a5.638 5.638 0 003.674-1.367l4.089 4.38a.27.27 0 00.388.009.29.29 0 00.008-.4zm-8.16-3.188c-2.875 0-5.215-2.41-5.215-5.373 0-2.962 2.34-5.372 5.216-5.372 2.876 0 5.215 2.41 5.215 5.372 0 2.963-2.34 5.373-5.215 5.373z"
                   stroke-width="1.2"></path>
                    </svg>
                </div>
                <? $APPLICATION->IncludeComponent("DVIGA:helpers.language.choose", "", []); ?>
                <div class="header__btn-menu-wrapper">
                    <button class="header__btn-menu" type="button">
                        <svg style="width: 18px; height: 12px; fill: none;">
                            <use href="#icon-burger"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </nav>
    </div>
    <div class="header__menu-block">
        <div class="all-links">
            <div class="all-links__top">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "header.menu-block",
                    array(
                        "ROOT_MENU_TYPE" => "header_upper",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "3",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                ); ?>
            </div>
            <div class="all-links__bottom">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "header.submenu_big",
                    array(
                        "ROOT_MENU_TYPE" => "header_lower",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "3",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                ); ?>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "header.menu-block",
                    array(
                        "ROOT_MENU_TYPE" => "header_lower",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "3",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                ); ?>
            </div>
            <? if (false) : ?>
                <div class="all-links__bottom">
                    <a href="#">О приморье</a>

                    <div data-event-list="leisure" class="all-links__wrapper all-links__wrapper--leisure">
                        <ul>
                            <li class="all-links__title-item">Чем заняться</li>
                            <li>
                                <a href="#">
                                    <span>Развлечения</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Зоопарки</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Зимний отдых</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Шоппинг</span>
                                </a>
                            </li>
                        </ul>

                        <ul>
                            <li class="all-links__title-item">Куда сходить</li>
                            <li>
                                <a href="#">
                                    <span>Рестораны и кафе</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Театры, музеи и галереи</span>
                                </a>
                            </li>
                        </ul>

                        <ul>
                            <li class="all-links__title-item">Достопримечательности</li>
                            <li>
                                <a href="#">
                                    <span>Горы и пещеры</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Религиозные объекты</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Острова и пляжи</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Заповедники</span>
                                </a>
                            </li>
                        </ul>

                        <ul>
                            <li class="all-links__title-item">Туры, экскурсии</li>
                            <li>
                                <a href="#">
                                    <span>По Владивостоку</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>По Приморскому краю</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Туры по Южному Приморью</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Экспедиция “Край тайги”</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Туризм для пожилых людей</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div data-event-list="hotels" class="all-links__wrapper">
                        <b class="all-links__title-item all-links__title-item--show-on">Где остановиться</b>
                        <ul>
                            <li>
                                <a href="#">
                                    <span>Отели и гостиницы</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Базы отдыха</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>Санатории</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "header.submenu_big",
                    array(
                        "ROOT_MENU_TYPE" => "header_upper",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "3",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                ); ?>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "header.submenu_big",
                    array(
                        "CLASS_MODIFIER" => "all-links__wrapper--leisure",
                        "ROOT_MENU_TYPE" => "header_lower",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "3",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                ); ?>
                <div class="all-links__wrapper">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "header.submenu_small",
                        array(
                            "ROOT_MENU_TYPE" => "header_upper",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MAX_LEVEL" => "3",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N"
                        ),
                        false
                    ); ?>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "header.submenu_small",
                        array(
                            "CLASS_MODIFIER" => "all-links__wrapper--hotels",
                            "ROOT_MENU_TYPE" => "header_lower",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MAX_LEVEL" => "3",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N"
                        ),
                        false
                    ); ?>
                </div>
            <? endif; ?>
        </div>
    </div>
    <transition name="opacity" v-cloak>
        <div class="container relative" v-show="searchShow">
            <form class="search__wrapper" action="<?= SITE_DIR ?>search/">
                <input type="text" class="search__input" name="q">
                <button type="submit" class="search__button"><?= getTranslation("SEARCH_TITLE"); ?></button>
            </form>
        </div>
    </transition>
</div>
</header>
<? if (!thisPageWithSectionImage() && $APPLICATION->GetCurPage() != SITE_DIR): ?>
    <div class="header-content header-content--bg-pattern-gray">
        <div class="container">
            <?php
            $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "",
                array(
                    "PATH" => "",
                    "SITE_ID" => "s1",
                    "START_FROM" => "0",
                ),
                false
            );
            ?>
            <? if(!withoutTitle()) :?>
                <h1 class="header-content__title header-content__title--color"><? $APPLICATION->ShowTitle(false) ?></h1>
            <? endif; ?>
        </div>
    </div>
<?php endif; ?>

<? if (showTypicalPage()): ?>
<div class="wrapper tours-inner">
    <main class="">
        <div class="container">
            <section class="tours-inner--content content content--narrow">
                <? endif; ?>
